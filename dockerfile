FROM ubuntu

RUN apt-get update && apt-get install -y build-essential

RUN mkdir /main

WORKDIR /main

COPY dockerizedServer.c .

COPY client.c .

COPY run_final.sh .

RUN chmod +x run_final.sh

CMD [ "/bin/bash","-c","./run_final.sh" ]