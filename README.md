# sisop-praktikum-fp-2023-HS-E10

## server.c

### UTILITY
### 1. set_server_daemon()
- untuk set program server menjadi daemon
```c
void set_server_daemon(){
    pid_t child_id;
    child_id = fork();

    if(child_id < 0){
        //fork failure
        exit(EXIT_FAILURE);
    }
    if(child_id > 0){
        //parent process
        exit(EXIT_SUCCESS);
    }

    umask(0);
    pid_t sid;
    sid = setsid();

    if(sid < 0){
        //set session id failure
        exit(EXIT_FAILURE);
    }
    if((chdir("/")) < 0){
        //set working directory to root, if failed exit
        exit(EXIT_FAILURE);
    }

    close(STDIN_FILENO);
    close(STDOUT_FILENO);
    close(STDERR_FILENO);
}
```
### 2. is_disconnected()
- untuk mengecek apakah client masih terhubung dengan server dengan mengecek return valread
- valread berisi jumlah bit yang dikirim dari client ke server
- jika valread = 0 maka tidak ada data yang dikirim, disconnect
- jika disconnect maka return 1
- jika tidak return 0
```c
bool is_disconnected(int valread, int* socket){
    if(valread == 0){
        close(*socket);
        return 1;
    }
    return 0;
}
```
### 3. is_db_exist()
- untuk mengecek apakah database ada pada database_and_permission_list
- lakukan read line by line kemudian bandingkan dengan nama database yang dicari
- jika ada return 1
- jika tidak ketemu return 0
```c
bool is_db_exist(char* database){
    FILE* file = fopen(database_and_permission_list,"r");

    char readDB[BUFFLEN];
    char readUser[BUFFLEN];

    if(file){
        while(fscanf(file, "%s %s", readDB, readUser) != EOF){
            if(!strcmp(readDB, database)){
                //db exist
                return 1;
            }
        }
    }
    //db not exist
    return 0;
}
```
### 4. is_user_exist()
- untuk mengecek apakah database ada pada user_list
- lakukan read line by line kemudian bandingkan dengan nama user yang dicari
- jika ada return 1
- jika tidak ketemu return 0
```c
bool is_user_exist(char* user){
    FILE *file = fopen(users_list, "r");

    char readUser[BUFFLEN];
    char readPassword[BUFFLEN];

    if(file){
        while(fscanf(file, "%s %s", readUser, readPassword) != EOF){
            if(!strcmp(readUser, user)){
                //user registered
                return 1;
            }
        }
    }
    //user not registered
    return 0;
}
```
### 5. trim()
- untuk remove leading space dari suatu string
- digunakan ketika tokenize query sehingga didapatkan nama column tanpa ada leading space
```c
char* trim(char *text){
    int index = 0;
    while(text[index] == ' ' || text[index] == '\t'){
        index++;
    }
    char *temp = strchr(text,text[index]);
    return temp;
}
```
### 6. append_to_file()
- untuk open append suatu file kemudian menambahkan content ke dalamnya
```c
void append_to_file(char fpath[BUFFLEN], char content[BUFFLEN]){
    FILE* file = fopen(fpath, "a");
    fprintf(file, "%s\n", content);
    fclose(file);
}
```
### 7. check_data_type()
- untuk menentukan data type ketika inserting
- jika terdeteksi ' ' maka string, return 1
- jika berisi angka maka int, return 2
- selain itu return 0, data type error, karena data type yang diperbolehkan hanya string dan int
```c
int check_data_type(char* buffer){
    char bufferCopy[BUFFLEN];
    strcpy(bufferCopy, buffer);
    char singleQuote = '\'';

    if(bufferCopy[0] == singleQuote && bufferCopy[strlen(bufferCopy)-1] == singleQuote){//string begins with ' ends with '
        return 1;
    }
    for(int i = 0; i < strlen(bufferCopy); i++){
        if(bufferCopy[i] < '0' || bufferCopy[i] > '9'){
            return 0; //data type error
        }
    }
    return 2; //integer
}
```
### 8. removeLastCharacter()
- untuk remove last character dari suatu string, dalam kasus ini '\n' atau ';', dengan cara mereplacenya dengan '\0'
```c
void removeLastCharacter(char* str) {
    int length = strlen(str);
    if (length > 0) {
        str[length - 1] = '\0';
    }
}
```
### FUNCTION
### 1. login()
- untuk autentikasi run program client.c
- mencocokkan username dan password dengan record pada user_list
- jika ada yang cocok maka login berhasil, return 1
- jika tidak ada login failed, return 0
```c
int login(char* user_type, char* username){
   
    FILE *file = fopen(users_list, "r");
    char user[BUFFLEN];
    char pass[BUFFLEN];

    char* token = strtok(user_type, " ");
    strcpy(user, token);
    token = strtok(NULL, " ");
    strcpy(pass, token);

    char readUser[BUFFLEN];
    char readPassword[BUFFLEN];
    if(file){
        while(fscanf(file, "%s %s", readUser, readPassword) != EOF){
            if(strcmp(readUser, user) == 0 && strcmp(readPassword, pass) == 0){ //username and password exist
                fclose(file);
                strcpy(username, user);
                return 1; //login succeed
            }
        }
        fclose(file);
    }
    return 0; //login failed, username and password does not match or not exist
}
```

### 2. create_user()
- cek apakah query diakhiri ';', jika tidak return -3 (missing semicolon)
- cek apakah user adalah root, jika tidak return 0 (only root may create user)
- tokenize query by space kemudian copy tiap kata ke dalam array
- cek kelengkapan syntax, jika ada yang error maka return -1 (syntax error)
- cek apakah user sudah terdaftar menggunakan fungsi is_user_exist()
- jika tidak ada masukkan username dan password ke user_list, return 1
- jika sudah ada return -2 (user already registered)
```c
int create_user(char* buffer, char* user_type){

    if(strcmp(user_type, "root") != 0){
        //only root may create user
        return 0;
    }

    size_t len = strlen(buffer);
    if (len > 0){
        char semicolon = buffer[len-2];
        if (semicolon != ';'){
            return -3;
        }
    }

    char bufferCopy[BUFFLEN] = {0};
    strcpy(bufferCopy, buffer);
   
    char* token = strtok(bufferCopy, ";");
    token = strtok(bufferCopy, " "); //CREATE
    char input[10][BUFFLEN];
    int i = 0;

    while (token != NULL) { //push word by word to an array of string
        strcpy(input[i],token);
        i++;
        token = strtok(NULL, " ");
    }

    // CREATE USER username IDENTIFIED BY password
    //    0    1      2         3      4     5
    if(strcmp("IDENTIFIED", input[3]) || strcmp("BY", input[4])){
        return -1; //syntax error
    }

    if (i < 6){
        return -1;
    }

    if(!is_user_exist(input[2])){ //if user not exist, create user
        char new_user[BUFFLEN];
        sprintf(new_user, "%s %s", input[2], input[5]);
        append_to_file(users_list, new_user);
        return 1; //user created
    }else {
        return -2; //user already registered
    }
}
```

### 3. grant_permission()
- cek apakah query diakhiri ';', jika tidak return -4 (missing semicolon)
- cek apakah user adalah root, jika tidak return 0 (only root may grant permission)
- tokenize query by space kemudian copy tiap kata ke dalam array
- cek kelengkapan syntax, jika ada yang error maka return -1 (syntax error)
- cek apakah database ada menggunakan is_db_exist()
- jika ada, cek apakah user terdaftar menggunakan is_user_exist()
- jika ada, append username dan password ke database_and_permission_list, return 1
- jika user tidak ada, return -3 (user not registered)
- jika database tidak ada, return -2 (database does not exist)
```c
int grant_pemission(char* buffer, char* user_type){

    size_t len = strlen(buffer);
    if (len > 0){
        char semicolon = buffer[len-2];
        if (semicolon != ';'){
            return -4;
        }
    }

    if(strcmp(user_type, "root")){
        return 0; //only root may grant permission
    }
    
    char bufferCopy[BUFFLEN] = {0};
    strcpy(bufferCopy, buffer);
   
    char* token = strtok(bufferCopy, ";");
    token = strtok(bufferCopy, " ");
    char input[10][BUFFLEN];

    int i = 0;
    while (token != NULL) { //put words in array
        strcpy(input[i], token);
        i++;
        token = strtok(NULL, " ");
    }

    if(i < 5 || strcmp("INTO", input[3])){
        return -1; //syntax error
    }

    // GRANT PERMISSION dbName INTO user
    //   0        1       2     3    4
    char grant[BUFFLEN] = {0};
    if(is_db_exist(input[2])){
        if(is_user_exist(input[4])){
            sprintf(grant, "%s %s", input[2], input[4]);
            append_to_file(database_and_permission_list,grant);
            return 1; //granted
        }else{
            return -3; //user not exist
        }
    }else{
        return -2; //database not exist
    }
}
```

### 4. create_database()
- cek apakah query diakhiri ';', jika tidak return -2 (missing semicolon)
- tokenize query sampai ke nama database
- jika token == NULL maka query kurang argument, return -1 (syntax error)
- jika token != NULL maka mkdir dengan nama database
- jika mkdir berhasil maka append nama database dan username yang melakukan create ke dalam database_and_permission_list, return 1
- jika tidak berhasil maka database sudah ada, return 0 (database already existed)
```c
int create_database(char* buffer, char* user_type, char* username){

    size_t len = strlen(buffer);
    if (len > 0){
        char semicolon = buffer[len-2];
        if (semicolon != ';'){
            return -2;
        }
    }

    char bufferCopy[BUFFLEN] = {0};
    char databaseName[BUFFLEN] = {0};
    strcpy(bufferCopy, buffer);
    
    char* token = strtok(bufferCopy, ";");
    token = strtok(bufferCopy, " "); //CREATE
    token = strtok(NULL, " "); //DATABASE
    token = strtok(NULL, " "); //dbName

    if(token != NULL){
        strcpy(databaseName, token);
    }else{
        return -1; //syntax error, expected database name
    }

    char fpath[BUFFLEN] = {0};
    sprintf(fpath, "%s/databases/%s", workspace, databaseName);
    
    if(mkdir(fpath, 0777) == 0){//database creation succeed
        char new_db[BUFFLEN] = {0};
        sprintf(new_db, "%s %s", databaseName, username);
        append_to_file(database_and_permission_list, new_db); //add db name and creator to list
        return 1;
    }else{
        return 0; //database already existed
    }
}
```

### 5. use()
- cek apakah query diakhiri ';', jika tidak return -3 (missing semicolon)
- tokenize query sampai ke nama database
- jika token == NULL maka query kurang argument, return -1 (syntax error)
- jika token != NULL maka cek apakah user punya akses ke database dengan membandingkan username dengan record pada database_and_permission_list
- jika ada, copy nama database ke use_database, return 1
- jika tidak ada, return 0 (permission denied)
- jika database tidak ada, return -2 (database does not exist)
```c
int use(char* buffer, char* user_type, char* username, char* use_database){

    size_t len = strlen(buffer);
    if (len > 0){
        char semicolon = buffer[len-2];
        if (semicolon != ';'){
            return -3;
        }
    }

    char bufferCopy[BUFFLEN] = {0};
    char databaseName[BUFFLEN] = {0};
    strcpy(bufferCopy, buffer);
    
    char* token = strtok(bufferCopy, ";");
    token = strtok(bufferCopy," "); //USE
    token = strtok(NULL, " "); //dbName
    
    if(token != NULL){
        strcpy(databaseName, token);
    }else{
        return -1; //syntax error, expected database name
    }

    FILE* file = fopen(database_and_permission_list, "r");

    char readDB[BUFFLEN];
    char readUser[BUFFLEN];
    bool db_exist = false;

    if(file){
        while(fscanf(file, "%s %s", readDB, readUser) != EOF){
            if(strcmp(readDB, databaseName) == 0){
                db_exist = true;
                if(strcmp(readUser, username) == 0 || strncmp(user_type, "root", 4) == 0){
                    fclose(file);
                    strcpy(use_database, databaseName);
                    return 1; //use database
                }
            }
        }
    }
    if(!db_exist){
        return -2; //database not exist
    }
    return 0;
}
```
### 6. drop_database()
- cek apakah query diakhiri ';', jika tidak return -3 (missing semicolon)
- tokenize query sampai ke nama database
- jika token == NULL maka query kurang argument, return -1 (syntax error)
- cek apakah user punya akses ke database yang akan di drop
- jika punya maka recursive remove directory database beserta isinya
- kemudian buat file new_database_and_permission_list dan copy semua line pada database_and_permission_list kecuali untuk nama database yang sudah di drop
- memset use_database menjadi NULL
```c
int drop_database(char* buffer, char* tipe, char* username, char* use_database){

    size_t len = strlen(buffer);
    if (len > 0){
        char semicolon = buffer[len-2];
        if (semicolon != ';'){
            return -3;
        }
    }

    char bufferCopy[BUFFLEN] = {0};
    strcpy(bufferCopy, buffer);
    
    char* token = strtok(bufferCopy, ";");
    token = strtok(bufferCopy, " "); //DROP
    token = strtok(NULL, " "); //DATABASE
    token = strtok(NULL, " "); //dbName
    
    char database[BUFFLEN] = {0};
    if(token != NULL){
        strcpy(database, token);
    }else{
        return -1; //syntax error, expected database name
    }

    char readDB[BUFFLEN];
    char readUser[BUFFLEN];
    bool is_db_exist = false;
    bool is_user_has_access =false;

    FILE* file = fopen(database_and_permission_list, "r");

    if(file){
        while(fscanf(file, "%s %s", readDB, readUser) != EOF){
            if(strcmp(readDB, database) == 0){
                is_db_exist = true;
                if(strcmp(readUser, username) == 0){
                    fclose(file);
                    is_user_has_access = true;
                    break;
                }
            }
        }

        if(is_user_has_access || (is_db_exist && strcmp(username, "root") == 0)){
            //deleting database
            char fpath[BUFFLEN] = {0};
            sprintf(fpath, "%s/databases/%s", workspace, database);
            char command[BUFFLEN];
            sprintf(command, "rm -rf %s", fpath);
            system(command);
            
            FILE* file_read = fopen(database_and_permission_list, "r");
            char new_database_and_permission_list[BUFFLEN] = {0};
            sprintf(new_database_and_permission_list, "%s/databases/administrator/temp.txt", workspace);

            FILE* file_write = fopen(new_database_and_permission_list, "w");
            while(fscanf(file_read, "%s %s", readDB, readUser) != EOF){
                if(strcmp(readDB, database)){ //if dbName != deleted-database name, append
                    char record[BUFFLEN] = {0};
                    fprintf(file_write, "%s %s\n", readDB, readUser);
                }
            }

            fclose(file_write);
            remove(database_and_permission_list);
            rename(new_database_and_permission_list, database_and_permission_list);

            if(strcmp(use_database, database) == 0){
                memset(use_database, 0, sizeof(use_database));
            }
            return 1; //drop database succeed
        }        
    }

    if(!is_db_exist){
        return -2; //database does not exist
    }

    return 0;
}
```
### 7. create_table()
- cek apakah query diakhiri ';', jika tidak return -5 (missing semicolon)
- cek apakah use_database == 0, jika iya maka belum USE database, return -2
- tokenize sampai didapatkan nama tabel kemudian simpan
- lanjutkan tokenize menggunakan strtok dan strchr sampai didapatkan nama per kolom dan data type kemudian simpan dalam array
- cek apakah data type string atau int, jika bukan keduanya maka return -4
- cek apakah tabel sudah ada menggunakan fopen mode r, jika sudah ada maka return 0
- jika tidak ada, fopen mode w untuk membuat struktur table kemudian append nama kolom dan data type pada array
```c
int create_table(char* buffer, char* use_database){

    size_t len = strlen(buffer);
    if (len > 0){
        char semicolon = buffer[len-2];
        if (semicolon != ';'){
            return -5;
        }
    }

    if(strlen(use_database) == 0){
        return -2;
    }

    char bufferCopy[BUFFLEN] = {0};
    char tableName[BUFFLEN] = {0};
    strcpy(bufferCopy, buffer);
    
    char* token = strtok(bufferCopy, ";");
    token = strtok(bufferCopy, " "); //CREATE
    token = strtok(NULL, " "); //TABLE
    token = strtok(NULL, " "); //tableName

    if(token != NULL){
        strcpy(tableName, token);
    }else{
        return -1; //syntax error, expected table name
    }

    char* temp;
    temp = strchr(buffer, '('); // (col1 string, col2 int);
    if(!temp){
        return -1; //missing left parentheses
    }else{
        temp = temp + 1; //shift to character after '('
    }

    char attributes[BUFFLEN] = {0};
    strcpy(attributes, temp);

    char* token2 = strtok(attributes, ";"); // col1 string, col2 int)
    token2 = strtok(attributes, ")"); // col1 string, col2 int

    char attr_array[100][BUFFLEN] = {0};
    int attr_count = 0;
    token2 = strtok(attributes, ",");

    while(token2 != NULL){
        strcpy(attr_array[attr_count], trim(token2));
        attr_count++;
        token2 = strtok(NULL, ",");
    }

    for(int i = 0; i < attr_count; i++){
        char temp2[BUFFLEN] = {0}; 
        char attr_name[BUFFLEN] = {0};
        char data_type[BUFFLEN] = {0};

        strcpy(temp2, attr_array[i]);
        char* token3 = strtok(temp2, " "); // col1
        strcpy(attr_name, token3);
        token3 = strtok(NULL, " "); // string or int

        if(token3){
            strcpy(data_type, token3);
            if(strcmp(data_type, "int") != 0 && strcmp(data_type, "string") != 0){
                return -4; //data type not allowed, only string or int allowed
            }
        }else{
            return -3; //syntax error, incomplete attribute declaration
        }
    }

    char fpath[BUFFLEN] = {0};
    sprintf(fpath, "%s/databases/%s/%s.txt", workspace, use_database, tableName);

    FILE* file = fopen(fpath, "r"); //attempt to open read table
    if(!file){
        file = fopen(fpath, "w");
        char attr_list_path[BUFFLEN] = {0};
        sprintf(attr_list_path, "%s/databases/%s/%s-attributes.txt", workspace, use_database, tableName);

        for(int i = 0; i < attr_count; i++){
            append_to_file(attr_list_path, attr_array[i]);
        }
        return 1; //table creation succeed
    }else{
        return 0; //open read succeed, table existed, cannot create table with same name
    }
}
```
### 8. drop_table()
- cek apakah query diakhiri ';', jika tidak return -3 (missing semicolon)
- cek apakah use_database == 0, jika iya maka belum USE database, return -2
- tokenize sampai didapatkan nama tabel kemudian simpan
- cek apakah table ada dengan mencoba fopen r, jika berhasil maka table ada
- jika ada, remove file table dan struktur tablenya
```c
int drop_table(char* buffer, char* use_database){

    size_t len = strlen(buffer);
    if (len > 0){
        char semicolon = buffer[len-2];
        if (semicolon != ';'){
            return -3;
        }
    }

    if(!strlen(use_database)){
        return -2; //syntax error expected database name
    }

    char bufferCopy[BUFFLEN];
    strcpy(bufferCopy, buffer);
    char* token = strtok(bufferCopy, ";"); //DROP TABLE tableName
    token = strtok(token, " "); //DROP
    token = strtok(NULL, " "); //TABLE
    token = strtok(NULL, " "); //tableName

    char table_attr_path[BUFFLEN] = {0};
    sprintf(table_attr_path, "%s/databases/%s/%s-attributes.txt", workspace, use_database, token);

    char table_path[BUFFLEN]={0};
    sprintf(table_path, "%s/databases/%s/%s.txt", workspace, use_database, token);

    FILE* file;
    file = fopen(table_attr_path, "r"); //attempting to open read table_attr to check if its exist
    if(file){
        fclose(file);
        remove(table_attr_path);
        remove(table_path);
        return 1; //drop table succeed
    }
    else{
        return -1; //table does not exist
    }
}
```

### 9. drop_column()
- cek apakah query diakhiri ';', jika tidak return -4 (missing semicolon)
- cek apakah use_database == 0, jika iya maka belum USE database, return -2
- tokenize query by space kemudian copy tiap kata ke dalam array
- cek kelengkapan syntax, jika ada yang error maka return -1 (syntax error)
- retrieve index column yang akan diremove
- buat file struktur tabel baru dengan cara mengcopy semua struktur dari tabel lama kecuali kolom dengan index yang di remove
- copy semua record dari tabel lama ke tabel baru kecuali kolom dengan index yang di remove
- remove file lama
- rename file baru menjadi file lama
```c
int drop_column(char* buffer, char* use_database){

    size_t len = strlen(buffer);
    if (len > 0){
        char semicolon = buffer[len-2];
        if (semicolon != ';'){
            return -4;
        }
    }

    if(!strlen(use_database)){
        return -2;
    }

    char input[10][BUFFLEN];
    char bufferCopy[BUFFLEN];
    strcpy(bufferCopy, buffer);

    int k = 0;
    char* token = strtok(bufferCopy, ";"); //DROP COLLUMN colName FROM tableName
    token = strtok(token, " "); //DROP
    while (token != NULL) {
        strcpy(input[k], token);
        k++;
        token = strtok(NULL, " ");
    }
    // DROP COLLUMN colName FROM tableName
    //  0      1       2     3       4

    if(k != 5){
        return -3; //not enough argument or syntax error
    }

    FILE *strukturin, *strukturout, *tablein, *tableout;

    char open[BUFFLEN]={0}, append[BUFFLEN]={0}, a[BUFFLEN]={0}, b[BUFFLEN]={0};
    sprintf(open, "%s/databases/%s/%s-attributes", workspace, use_database, input[4]);
    strcpy(a,open);
    strcat(a,"2.txt");
    strcat(open,".txt");

    strukturin = fopen(open,"r");
   
    int i = 0,j = 0;
    char data_type[1000], name[1000];

    if(strukturin){
        strukturout = fopen(a,"w");
        while(fscanf(strukturin,"%s %s",name,data_type) != EOF){
            if(strcmp(input[2],name)){
                fprintf(strukturout,"%s %s\n",name,data_type);
            }
            else i = j;
            j++;
        }
        fclose(strukturin);
        fclose(strukturout);
    }
    else{
        return -1;
    }

    sprintf(append,"%s/databases/%s/%s",workspace,use_database,input[4]);
    strcpy(b,append);
    strcat(b,"2.txt");
    strcat(append,".txt");

    tablein = fopen(append,"r");
    tableout = fopen(b,"w");

    char ambil[1000];
    while(fgets(ambil,1000,tablein)){
        token = strtok(ambil,",");
        int j = 0;
        char baru[1000];
        strcpy(baru,"");
        while(token!=NULL){
            if(j!=i){
                strcat(baru,token);
                strcat(baru,",");
            }
            token = strtok(NULL,",");
            j++;
        }
        baru[strlen(baru)-1] = 0;
        fprintf(tableout,"%s",baru);
        if(baru[strlen(baru)-1] != '\n'){
            fprintf(tableout,"\n");
        }
    }
    fclose(tablein);
    fclose(tableout);
    remove(open);
    rename(a,open);
    remove(append);
    rename(b,append);
    return 1;
}
```

### 10. insert()
- cek apakah query diakhiri ';', jika tidak return -5 (missing semicolon)
- cek apakah use_database == 0, jika iya maka belum USE database, return -1
- tokenize sampai didapatkan nama tabel kemudian simpan
- lanjutkan tokenize menggunakan strtok dan strchr sampai didapatkan nama per kolom dan data type kemudian simpan dalam array
- cek apakah data type sudah sesuai dengan struktur table
- append record ke dalam file table
```c
int insert(char* buffer, char* use_database){

    size_t len = strlen(buffer);
    if (len > 0){
        char semicolon = buffer[len-2];
        if (semicolon != ';'){
            return -5;
        }
    }

    if(!strlen(use_database)){
        return -1; //syntax error, expected database name
    }

    char bufferCopy[BUFFLEN];
    strcpy(bufferCopy, buffer);

    char *token = strchr(bufferCopy, '('); // (val1, val2)
    token = token + 1; // val1, val2)
    token = strtok(token, ")"); // val1, val2
    token = strtok(token, ","); // val1

    char data[100][BUFFLEN];

    int i = 0;

    while(token!=NULL){
        strcpy(data[i], trim(token));
        i++;
        token = strtok(NULL, ",");
    }

    FILE* fileRead;

    strcpy(bufferCopy, buffer);
    token = strtok(bufferCopy," "); //INSERT
    token = strtok(NULL, " "); //INTO
    token = strtok(NULL, " "); //tableName

    char fpath[BUFFLEN] = {0};
    sprintf(fpath, "%s/databases/%s/%s-attributes.txt", workspace, use_database, token);
    fileRead = fopen(fpath, "r");
   
    char data_type[100][BUFFLEN];
    char readDataType[BUFFLEN];
    char value[BUFFLEN];
    int k = 0;

    if(fileRead){
        while(fscanf(fileRead, "%s %s", value, readDataType) != EOF){
            strcpy(data_type[k], readDataType);
            k++;
        }
    }
    else{
        return -2; //table does not exist
    }

    FILE *fileWrite;
    char tablePath[BUFFLEN] = {0};
    sprintf(tablePath, "%s/databases/%s/%s.txt", workspace, use_database, token);
    fileWrite = fopen(tablePath, "a");

    if(k != i){
        fclose(fileRead);
        fclose(fileWrite);
        return -3; //not enough argument passed
    }

    for(int j = 0; j < i; j++){
        int val = check_data_type(data[j]);
        if(val == 1 && !strcmp(data_type[j], "string"));
        else if (val == 2 && !strcmp(data_type[j], "int"));
        else {
            fclose(fileRead);
            fclose(fileWrite);
            return -4; //data type error
        }  
    }
    for(int j = 0; j < i-1; j++){
        fprintf(fileWrite, "%s,", data[j]); //inserting data, comma separated
    }
    fprintf(fileWrite,"%s\n", data[i-1]);
    fclose(fileRead);
    fclose(fileWrite);

    return 1; //insertion succeed
}
```

### 11. delete_from()
- cek apakah query diakhiri ';', jika tidak return -5 (missing semicolon)
- cek apakah use_database == 0, jika iya maka belum USE database, return -1
- tokenize query by space kemudian copy tiap kata ke dalam array
- jika tidak ada statement WHERE maka semua record dihapus dengan cara fopen mode w
- jika ada statement WHERE maka dapatkan index column WHERE
- buat file table baru kemudian copy semua record kecuali jika record pada index WHERE sesuai dengan value WHERE
- remove file table lama
- rename tile table baru menjadi file table lama
```c
int delete_from(char* buffer, char* use_database){

    size_t len = strlen(buffer);
    if (len > 0){
        char semicolon = buffer[len-2];
        if (semicolon != ';'){
            return -5;
        }
    }

    if(!strlen(use_database)){
        return -1;
    }

    char bufferCopy[BUFFLEN] = {0};
    strcpy(bufferCopy, buffer);
   
    char* token = strtok(bufferCopy, ";"); //DELETE FROM tableName
    token = strtok(bufferCopy," "); //DELETE
    char input[10][BUFFLEN];
    int i = 0;

    while (token != NULL) {
        strcpy(input[i], token);
        i++;
        token = strtok(NULL, " ");
    }
    //DELETE FROM tableName
    //   0     1      2
    if(i == 3){
        //no where statement, clear all records
        char fpath[BUFFLEN];
        sprintf(fpath, "%s/databases/%s/%s.txt", workspace, use_database, input[2]);
        FILE* file = fopen(fpath, "r");
        if(file){
            fclose(file);
            file = fopen(fpath, "w"); //using write on existed file effectively rewrite all records
            fprintf(file, "");
            fclose(file);
        }
        else{
            return -3; //open read failed, table does not exist
        }
        return 1; //delete succeed
    
    //DELETE FROM tableName WHERE colName=Value
    //  0     1       2       3         4
    }else if(i == 5){
        if(strcmp(input[3], "WHERE")){
            return -2; //syntax error, missing WHERE statement
        }

        char whereCol[BUFFLEN];
        char whereVal[BUFFLEN];

        token = strtok(input[4], "="); //colname
        strcpy(whereCol, token);
        token = strtok(NULL, "="); //value
        strcpy(whereVal, token);

        char attr_path[BUFFLEN];
        sprintf(attr_path, "%s/databases/%s/%s-attributes.txt", workspace, use_database, input[2]);
        FILE* file = fopen(attr_path, "r");

        int where_col_idx = -1;
        if(file){
            int count = 0;
            char readCol[BUFFLEN];
            char readDataType[BUFFLEN];
            while(fscanf(file, "%s %s", readCol, readDataType) != EOF){
                if(strcmp(whereCol, readCol) == 0){
                    where_col_idx = count;
                }
                count++;
            }

            if(where_col_idx == -1){
                fclose(file);
                return -4; //no colName matches whereColName
            }
            fclose(file);
        }else{
            return -3; //table does not exist
        }

        char fpath[BUFFLEN];
        char new_fpath[BUFFLEN];

        sprintf(fpath, "%s/databases/%s/%s.txt", workspace, use_database, input[2]);
        sprintf(new_fpath, "%s/databases/%s/%s_copy.txt", workspace, use_database, input[2]);

        FILE* file_read = fopen(fpath, "r");
        if(file_read){
            char record[BUFFLEN];
            FILE* file_write = fopen(new_fpath, "w");

            while(fgets(record, BUFFLEN, file_read)){
                char recordCopy[BUFFLEN];
                strcpy(recordCopy, record);
                removeLastCharacter(recordCopy); //remove \n 

                token = strtok(recordCopy, ","); // val1
                char new_record[BUFFLEN] = "";
                bool is_deleted = false;

                int curCol = 0;
                while(token != NULL){
                    strcat(new_record, token);
                    strcat(new_record, ",");

                    if(curCol == where_col_idx){
                        if(strcmp(whereVal, token) == 0){
                            is_deleted = true;
                        }
                    }
                    token = strtok(NULL, ","); //nextVal
                    curCol++;
                }

                if(!is_deleted){ //if not deleted append to file
                    removeLastCharacter(new_record); //remove tailing comma
                    strcat(new_record, "\n");
                    fprintf(file_write, "%s", new_record);
                }
            }
            fclose(file_read);
            fclose(file_write);
            remove(fpath);
            rename(new_fpath, fpath);
            return 1; //deletion succeed

        }else{
            return -3; //table does not exist
        }

    }else{
        return -2; //syntax error, argument does not match
    }
}
```

### 12. update()
- cek apakah query diakhiri ';', jika tidak return -5 (missing semicolon)
- cek apakah use_database == 0, jika iya maka belum USE database, return -1
- tokenize query by space kemudian copy tiap kata ke dalam array
- dapatkan index kolom yang mau diupdate
- jika ada WHERE maka dapatkan index kolom WHERE
- buat file table baru
- buat variable record dan new_record, record adalah record lama sedangkan new_record adalah record setelah diupdate
- new_record didapatkan dengan cara strcat value per kolom dari record lama, jika pada index WHERE valuenya sesuai maka yang di strcat adalah value baru
- jika tidak ada statement WHERE maka semua record pada index update diganti menjadi value baru
- masukkan masing-masing record (tidak ada update pada record tersebut) atau new_record (ada update) ke file table baru
- remove file table lama
- rename file table baru menjadi file table lama
```c
int update(char* buffer, char* use_database){

    size_t len = strlen(buffer);
    if (len > 0){
        char semicolon = buffer[len-2];
        if (semicolon != ';'){
            return -5;
        }
    }

    if(!strlen(use_database)){
        return -1;
    }

    char bufferCopy[BUFFLEN] = {0};
    strcpy(bufferCopy, buffer);

    char* token = strtok(bufferCopy, ";"); //UPDATE tableName SET colName = value
    token = strtok(bufferCopy, " "); //UPDATE
    char input[10][BUFFLEN];
    int i = 0;

    while (token != NULL) {
        strcpy(input[i], token);
        i++;
        token = strtok(NULL, " ");
    }

    //UPDATE tableName SET colName=value
    //  0        1      2        3
    if(strcmp(input[2], "SET")){
        return -2; //syntax error
    }

    char colName[BUFFLEN];
    char newVal[BUFFLEN];

    token = strtok(input[3], "="); //colName
    strcpy(colName, token);
    token = strtok(NULL, "="); //value
    strcpy(newVal, token);

    char attr_path[BUFFLEN];
    sprintf(attr_path, "%s/databases/%s/%s-attributes.txt", workspace, use_database, input[1]);
    FILE *file = fopen(attr_path, "r");

    int col_idx = -1;
    int count = 0;
    char readDataType[BUFFLEN];
    char readColName[BUFFLEN];
    if(file){
        while(fscanf(file, "%s %s", readColName, readDataType) != EOF){
            if(strcmp(colName, readColName) == 0){
                col_idx = count;
            }
            count++;
        }
        if(col_idx == -1){
            fclose(file);
            return -4; //attribute does not exist
        }
        fclose(file);
    
    }else{
        return -3; //table does not exist
    }

    //WHERE statement
    int where_col_idx = -1;
    char whereVal[BUFFLEN];

    //UPDATE tableName SET colName=value WHERE colName=value
    //  0        1      2         3        4         5
    if(input[4] != NULL && strcmp(input[4], "WHERE") == 0){ //execute if input[4] = WHERE
        char whereColName[BUFFLEN];
        token = strtok(input[5], "="); //colName
        strcpy(whereColName, token);
        token = strtok(NULL, "="); //value
        strcpy(whereVal, token);
        
        FILE* fileWhere = fopen(attr_path, "r");
        char readWhereColName[BUFFLEN];
        char readWhereDataType[BUFFLEN];
        int whereCount = 0;
        while(fscanf(fileWhere, "%s %s", readWhereColName, readWhereDataType) != EOF){
            if(strcmp(whereColName, readWhereColName) == 0){
                where_col_idx = whereCount;
            }
            whereCount++;
        }
        if(where_col_idx == -1){
            fclose(fileWhere);
            return -4; //where attribute not exist
        }
        fclose(fileWhere);
    }

    char fpath[BUFFLEN];
    char new_fpath[BUFFLEN];
    sprintf(fpath, "%s/databases/%s/%s.txt", workspace, use_database, input[1]);
    sprintf(new_fpath, "%s/databases/%s/%s_copy.txt", workspace, use_database, input[1]);

    FILE* file_read;
    FILE* file_write;
    file_read = fopen(fpath, "r");
    file_write = fopen(new_fpath, "w");

    if(file_read){
        char record[BUFFLEN];

        while(fgets(record, BUFFLEN, file_read) != NULL){
            
            char recordCopy[BUFFLEN];
            strcpy(recordCopy, record);
            removeLastCharacter(recordCopy);

            token = strtok(recordCopy, ","); // val1
            int curCol = 0;
            char new_record[BUFFLEN] = "";
            bool is_where = false;

            while(token != NULL){
                if(curCol == where_col_idx){
                    if(strcmp(whereVal, token) == 0){
                        is_where = true; //where found
                    }
                }
                //creating new record
                if(curCol == col_idx){
                    strcat(new_record, newVal); //updated value in said query
                    strcat(new_record, ",");
                }else{
                    strcat(new_record, token); //everything else
                    strcat(new_record, ",");
                }
                token = strtok(NULL, ","); //next val
                curCol++;
            }
            //if no update statement -> all record modified
            //if there is update statement and current record match the where statement -> append new record
            if(is_where == true || where_col_idx == -1){
                removeLastCharacter(new_record); //replace tailing comma with null to end record
                strcat(new_record, "\n");
                fprintf(file_write, "%s", new_record);

            //else append original record
            }else{
                fprintf(file_write, "%s", record);
            }
        }
        fclose(file_read);
        fclose(file_write);
        remove(fpath);
        rename(new_fpath, fpath);
        return 1; //update succeed
    }
}
```

### 13. select_table()
- cek apakah query diakhiri ';', jika tidak return -5 (missing semicolon)
- cek apakah use_database == 0, jika iya maka belum USE database, return -1
- tokenize query by space kemudian copy tiap kata ke dalam array
- jika terdapat WHERE maka dapatkan index column WHERE
- jika bukan SELECT *, maka dapatkan index-index column yang ingin diselect kemudian simpan dalam array
- jika * tanpa WHERE maka send setiap record ke socket
- jika * + WHERE maka cocokkan value pada index WHERE dengan record, jika cocok maka send record ke socket
- jika bukan * tanpa WHERE maka buat string baru dengan urutan sesuai kolom yang diminta kemudian send ke socket
- jika bukan * + WHERE maka cocokkan value pada index WHERE dengan record, jika cocok maka buat string baru sesuai urutan kolom yang diminta kemudian send ke socket
```c
int select_table(char* buffer, char* use_database, int* new_socket){
    if(!strlen(use_database)){
        return -1;
    }

    size_t len = strlen(buffer);
    if (len > 0){
        char semicolon = buffer[len-2];
        if (semicolon != ';'){
            return -5;
        }
    }
    
    char bufferCopy[BUFFLEN] = {0};
    strcpy(bufferCopy, buffer);
   
    char* token = strtok(bufferCopy, ";"); //SELECT colNames FROM tableName
    token = strtok(bufferCopy, " "); //SELECT
    char input[10][BUFFLEN] = {0};
    int argc = 0;

    while (token != NULL) {
        strcpy(input[argc], token);
        argc++;
        token = strtok(NULL, " ");
    }

    bool star = false;
    int where_col_idx = -1;
    int FROM_idx;
    int selected_cols[20];
    char whereCol[BUFFLEN];
    char whereVal[BUFFLEN];
    char attr_path[BUFFLEN];

    FILE* read_attr;

    //star
    //SELECT * FROM tableName
    //  0    1  2       3
    if(strcmp(input[1], "*") == 0){
        sprintf(attr_path, "%s/databases/%s/%s-attributes.txt", workspace, use_database, input[3]);

        FILE* read_attr = fopen(attr_path, "r");
        if(!read_attr){
            return -3; //table does not exist
        }
        fclose(read_attr); //table exist, continue

        if(strcmp(input[2], "FROM")){
            return -2; //syntax error, missing FROM after *
        }

        star = true;
        FROM_idx = 2;

        //SELECT * FROM tableName WHERE colName=value
        //  0    1   2      3       4         5
        if(input[4] != NULL && strcmp(input[4], "WHERE") == 0){

            token = strtok(input[5], "="); //whereCol
            strcpy(whereCol, token);
            token = strtok(NULL, "="); //whereVal
            strcpy(whereVal, token);

            if(strcmp(input[4], "WHERE") == 0){
                read_attr = fopen(attr_path, "r");
                char readCol[BUFFLEN];
                char readDataType[BUFFLEN];
                int k = 0;
                while(fscanf(read_attr, "%s %s", readCol, readDataType) != EOF){
                    if(strcmp(whereCol, readCol) == 0){
                        where_col_idx = k; //retrieve where_col_idx
                    }
                    k++;
                }
                if(where_col_idx == -1){
                    fclose(read_attr); 
                    return -4; //whereCol not exist
                }
                fclose(read_attr);
            }
        }
    
    //colls
    //SELECT col1, ..., coln FROM tableName
    //  0     1           n   n+1    n+2
    }else{
        int cur_idx = 0;
        while(cur_idx < argc && strcmp(input[cur_idx], "FROM")){
            cur_idx++; //getting idx of FROM statement, end of colNames
        }
        FROM_idx = cur_idx;

        if(FROM_idx == argc){
            return -2; //syntax error, missing FROM statement or table name
        }

        int tableName_idx = FROM_idx + 1;
        sprintf(attr_path, "%s/databases/%s/%s-attributes.txt", workspace, use_database, input[tableName_idx]);
        read_attr = fopen(attr_path, "r");
        if(!read_attr){
            return -3; //table does not exist
        }
        fclose(read_attr);
        
        for(int x = 0; x < 20; x++){
            selected_cols[x] = -1;
        }

        cur_idx = 1; //after SELECT
        int col_count = 0;
        int col_found = 0;
        while(cur_idx < FROM_idx){
            col_count++;
            read_attr = fopen(attr_path, "r");
            char readCol[BUFFLEN];
            char readDataType[BUFFLEN]; //the input was tokenized by space
            char readColWithComma[BUFFLEN]; //so if the selected col is more than 1 then some of them still contains comma

            int attr_idx = 0;
            while(fscanf(read_attr, "%s %s", readCol, readDataType) != EOF){
                strcpy(readColWithComma, readCol);
                strcat(readColWithComma, ",");

                if(strcmp(input[cur_idx], readCol) == 0 || strcmp(input[cur_idx], readColWithComma) == 0){
                    selected_cols[col_found] = attr_idx;
                    col_found++;
                }
                attr_idx++;
            }

            if(col_count != col_found){ //if after searching col_found is not incremented means there is a col not found, quit
                fclose(read_attr);
                return -4; //col does not exist
            }
            fclose(read_attr);
            cur_idx++;
        }
        
        //SELECT col1, ..., coln FROM tableName WHERE col=Value
        //  0     1          n    n+1    n+2     n+3     n+4
        int WHERE_idx = FROM_idx + 2;
        if(strcmp(input[WHERE_idx], "WHERE") == 0 && input[WHERE_idx + 1] != NULL){
            token = strtok(input[WHERE_idx + 1], "="); //whereColName
            strcpy(whereCol, token);
            token = strtok(NULL, "="); //whereColVal
            strcpy(whereVal, token);

            read_attr = fopen(attr_path, "r");
            char readCol[BUFFLEN];
            char readDataType[BUFFLEN];
            int attr_idx = 0;
            while(fscanf(read_attr, "%s %s", readCol, readDataType) != EOF){
                if(strcmp(whereCol, readCol) == 0){
                    where_col_idx = attr_idx;
                }
                attr_idx++;
            }

            if(where_col_idx == -1){
                fclose(read_attr);
                return -4; //where col does not exist
            }
            fclose(read_attr);
        }
    }

    char path[BUFFLEN];
    sprintf(path, "%s/databases/%s/%s.txt", workspace, use_database, input[FROM_idx+1]);
    
    FILE* file_read = fopen(path, "r");
    char record[BUFFLEN];

    if(file_read){
        while(fgets(record, BUFFLEN, file_read)){
            removeLastCharacter(record); //delete \n

            if(star){
                if(where_col_idx == -1){ //star no WHERE, print all
                    char msg[BUFFLEN];
                    sprintf(msg, "%s\n", record);
                    send(*new_socket , msg , strlen(msg) , 0 );

                }else{ //star + WHERE
                    char readCopy[BUFFLEN];
                    strcpy(readCopy, record);
                    token = strtok(readCopy, ",");
                    int curCol = 0;
                    while(token != NULL){
                        if(curCol == where_col_idx){
                            char whereValWithEndl[BUFFLEN];
                            sprintf(whereValWithEndl, "%s\n", whereVal);

                            if(strcmp(whereVal, token) == 0 || strcmp(whereValWithEndl, token) == 0){
                                char msg[BUFFLEN];
                                sprintf(msg, "%s\n", record);
                                send(*new_socket, msg , strlen(msg), 0 );
                                break;
                            }
                        }
                        token = strtok(NULL, ",");
                        curCol++;
                    }
                }

            }else{ //spesific column
                if (where_col_idx == -1){ //no WHERE
                    char msg[BUFFLEN] = "";
                    int idx = 0;
                    while (selected_cols[idx] != -1){
                        char readCopy[BUFFLEN];
                        strcpy(readCopy, record);
                        token = strtok(readCopy, ",");
                        int curCol = 0;
                        while(token != NULL){
                            if(curCol == selected_cols[idx]){
                                strcat(msg, token);
                                strcat(msg, ",");
                                break;
                            }
                            token = strtok(NULL, ",");
                            curCol++;
                        }                   
                        idx++;
                    }
                    removeLastCharacter(msg); //replace tailing comma with null
                    strcat(msg, "\n");
                    send(*new_socket , msg , strlen(msg) , 0 );

                }else{
                    //with WHERE
                    char msg[BUFFLEN] = "";
                    int idx = 0;
                    bool qualified = false;
                    while (selected_cols[idx] != -1){
                        char readCopy[BUFFLEN];
                        strcpy(readCopy, record);
                        token = strtok(readCopy, ",");
                        int curCol = 0;
                        while(token != NULL){
                            if(curCol == selected_cols[idx]){
                                strcat(msg, token);
                                strcat(msg, ",");
                                break;
                            }
                            token = strtok(NULL, ",");
                            curCol++;
                        }                   
                        idx++;
                    }
                    removeLastCharacter(msg); //replace tailing comma with null
                    strcat(msg, "\n");
                    send(*new_socket , msg , strlen(msg) , 0 );
                }              
            }
        }
        return 1;
    }
    else{
        return -2;
    }
}
```

### 14. append_log()
- untuk logging setiap query
```c
void append_log(char *login_user, char *command){
    FILE *file;
    char fpath[BUFFLEN] = {0};
    sprintf(fpath, "%s/dblog.log", workspace);
    file = fopen(fpath, "a");

    char time_now[100] = {0};
	time_t t = time(NULL);
	struct tm *tm = localtime(&t);
	strftime(time_now,100,"%Y-%m-%d %H:%M:%S",tm);
    fprintf(file, "%s:%s:%s\n", time_now, login_user, command);
	fclose(file);
}
```
### 15. play()
- untuk menerima query dari client kemudian run command sesuai dengan string query yang diterima
- setelah run command yang diminta maka akan memberikan reply ke client
```c
kepanjangan, bisa liaht di server.c
```
### 16. main()
```c
int main(int argc, char const *argv[]) {

    char dbpath[BUFFLEN] = {0};
    sprintf(dbpath,"%s/databases",workspace);
    mkdir(dbpath, 0777);

    int server_fd, new_socket[MAX_CLIENTS];
    struct sockaddr_in address;
    int opt = 1;
    int addrlen = sizeof(address);
      
    if ((server_fd = socket(AF_INET, SOCK_STREAM, 0)) == 0) {
        perror("socket failed");
        exit(EXIT_FAILURE);
    }
      
    if (setsockopt(server_fd, SOL_SOCKET, SO_REUSEADDR | SO_REUSEPORT, &opt, sizeof(opt))) {
        perror("setsockopt");
        exit(EXIT_FAILURE);
    }

    address.sin_family = AF_INET;
    address.sin_addr.s_addr = INADDR_ANY;
    address.sin_port = htons( PORT );
      
    if (bind(server_fd, (struct sockaddr *)&address, sizeof(address))<0) {
        perror("bind failed");
        exit(EXIT_FAILURE);
    }

    if (listen(server_fd, 3) < 0) {
        perror("listen");
        exit(EXIT_FAILURE);
    }
    set_server_daemon();
    int ctr = 0;
    while(1){
        if ((new_socket[ctr] = accept(server_fd, (struct sockaddr *)&address, (socklen_t*)&addrlen))<0) {
            perror("accept\n");
            exit(EXIT_FAILURE);
        }
        pthread_create(&(tid[ctr]),NULL,play,&new_socket[ctr]);
        ctr++;
        printf("Client %d terhubung\n", ctr);
    }
    return 0;
}
```

### Contoh Run
![](Dokumentasi/run1.png)
![](Dokumentasi/run2.png)
![](Dokumentasi/lib1.png)
![](Dokumentasi/lib2.png)
## client.c

### 1. checkClose
```c
- fungsi checkClose() yang digunakan untuk memeriksa apakah pembacaan (valread) dari socket menghasilkan nilai 0
- Jika nilai valread adalah 0, artinya koneksi telah ditutup oleh pihak lain (server) dan fungsi akan menutup soket (sock) 
dan mengembalikan nilai true (1)
- Jika nilai valread bukan 0, artinya koneksi masih terbuka dan fungsi mengembalikan nilai false (0)
```
![](Dokumentasi/checkClose.jpg)

### 2. mode_input
```c
- Mode_input digunakan dalam program untuk menentukan apakah program berjalan dalam mode input atau tidak
- Nilai false menunjukkan bahwa program tidak berjalan dalam mode input
- Nilai true menunjukkan bahwa program berjalan dalam mode input
```
![](Dokumentasi/mode_input.jpg)

### 3. User ID
```c
Bagian dari kondisional yang memeriksa ID pengguna (user ID) dan argumen baris perintah untuk menjalankan program dengan mode
yang sesuai. Terdapat dua mode: mode non-root user dan mode root user.
```

### 4. Non-Root User
```c
- Jika getuid() mengembalikan nilai bukan 0 (non-root user), maka kode dalam blok ini akan dieksekusi
- Pertama, jika jumlah argumen kurang dari 5, artinya tidak cukup argumen yang diberikan program akan mencetak pesan kesalahan 
"not enough arguments passed"
- Kedua, apakah argumen pada indeks 1 dan 3 sesuai dengan "-u" dan "-p". Jika tidak sesuai, program akan mencetak pesan kesalahan 
"syntax error"
- Ketiga, jika jumlah argumen adalah 7 (termasuk opsi "-d"), maka program akan memeriksa apakah argumen pada indeks 5 adalah 
"-d". Jika iya, maka mode_input akan diatur sebagai true, menandakan bahwa program berjalan dalam mode input. Jika tidak, 
program akan mencetak pesan kesalahan "syntax error"
```
![](Dokumentasi/Non-Root_User.jpg)

### 5. Root User
```c
- Jika getuid() mengembalikan nilai 0 (root user), maka kode dalam blok ini akan dieksekusi
- Kode yang berada di dalam blok ini akan mengambil jalur eksekusi yang berbeda tergantung pada kebutuhan program untuk root user
- Jika jumlah argumen baris perintah adalah 3, maka kode dalam blok ini akan dieksekusi
- Selanjutnya, kode memeriksa apakah argumen pada indeks 1 (argv[1]) adalah "-d" (mode debug). Jika ya, maka mode_input akan 
diatur sebagai true, menandakan bahwa program berjalan dalam mode input
- Jika argumen pada indeks 1 bukan "-d", program akan mencetak pesan kesalahan "syntax error"
```
![](Dokumentasi/Root_User.jpg)

### 6. socket
```c
- Berfungsi untuk menginisialisasi variabel yang diperlukan untuk pembuatan soket dan melakukan pengecekan apakah 
pembuatan soket berhasil atau tidak
- Jika pembuatan soket gagal, maka program akan mencetak pesan kesalahan dan keluar dengan nilai -1
```
![](Dokumentasi/socket.jpg)

### 7. Memory Server
```c
- Menginisialisasi alamat server yang akan dituju oleh soket (socket) dengan mengatur jenis alamat, nomor port, 
dan alamat IP yang ditentukan
- Jika ada kesalahan dalam konversi alamat IP atau alamat tidak valid, program akan mencetak pesan kesalahan dan keluar 
dengan nilai -1
```
![](Dokumentasi/Memory_Server.jpg)

### 8. Connection
```c
Koneksi dibuat dengan server menggunakan fungsi connect().
```
![](Dokumentasi/Connection.jpg)

### 9. User Information
```c
Informasi pengguna (nama pengguna dan kata sandi) dikirim ke server untuk otentikasi.
```
![](Dokumentasi/User_Information.jpg)

### 10. status_login
```c
- Respons server terhadap permintaan autentikasi diterima dan diperiksa
- Jika otentikasi gagal, koneksi ditutup dan program ditutup
```
![](Dokumentasi/status_login.jpg)

### 11. mode_input non-root user
```c
- Ditujukan untuk pengguna bukan root user (getuid() mengembalikan nilai selain 0) dan mode_input bernilai true
- Bagian ini bertujuan untuk mengirim perintah "USE" ke server untuk mengubah basis data aktif sesuai dengan argumen yang 
diberikan oleh pengguna melalui argv[6]
- Kemudian, perintah-perintah tambahan yang dimasukkan oleh pengguna melalui fscanf() dikirim ke server dan responsnya ditampilkan
```
![](Dokumentasi/mode_input_non-root_user.jpg)

### 12. mode_input root user
```c
- Ditujukan untuk pengguna adalah root user (tidak memiliki UID 0) dan mode_input bernilai true
- Bagian ini bertujuan untuk mengirim perintah "USE" ke server untuk mengubah basis data aktif sesuai dengan argumen 
yang diberikan oleh pengguna melalui argv[2]
- Kemudian, perintah-perintah tambahan yang dimasukkan oleh pengguna melalui fscanf() dikirim ke server dan responsnya ditampilkan
```
![](Dokumentasi/mode_input_root_user.jpg)

### 13. Message
```c
Sebuah loop dimulai untuk terus mengirim pesan dari pengguna ke server dan menerima tanggapan.
```
![](Dokumentasi/message.png)
