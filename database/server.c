#include <stdio.h>
#include <sys/socket.h>
#include <stdlib.h>
#include <netinet/in.h>
#include <string.h>
#include <unistd.h>
#include <pthread.h>
#include <stdbool.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <unistd.h>
#include <dirent.h>
#pragma GCC diagnostic ignored "-Wformat-overflow"
#pragma GCC diagnostic ignored "-Wformat-zero-length"
#define PORT 8080
#define MAX_CLIENTS 1000
#define BUFFLEN 1024

pthread_t tid[MAX_CLIENTS];

char* workspace = "/home/nizamhakims/workspace/PrakFinal";
char* users_list = "/home/nizamhakims/workspace/PrakFinal/databases/user_list.txt";
char* database_and_permission_list = "/home/nizamhakims/workspace/PrakFinal/databases/db_and_permission_list.txt";

void set_server_daemon(){
    pid_t child_id;
    child_id = fork();

    if(child_id < 0){
        //fork failure
        exit(EXIT_FAILURE);
    }
    if(child_id > 0){
        //parent process
        exit(EXIT_SUCCESS);
    }

    umask(0);
    pid_t sid;
    sid = setsid();

    if(sid < 0){
        //set session id failure
        exit(EXIT_FAILURE);
    }
    if((chdir("/")) < 0){
        //set working directory to root, if failed exit
        exit(EXIT_FAILURE);
    }

    close(STDIN_FILENO);
    close(STDOUT_FILENO);
    close(STDERR_FILENO);
}

bool is_disconnected(int valread, int* socket){
    if(valread == 0){
        close(*socket);
        return 1;
    }
    return 0;
}

bool is_db_exist(char* database){
    FILE* file = fopen(database_and_permission_list,"r");

    char readDB[BUFFLEN];
    char readUser[BUFFLEN];

    if(file){
        while(fscanf(file, "%s %s", readDB, readUser) != EOF){
            if(!strcmp(readDB, database)){
                //db exist
                return 1;
            }
        }
    }
    //db not exist
    return 0;
}

bool is_user_exist(char* user){
    FILE *file = fopen(users_list, "r");

    char readUser[BUFFLEN];
    char readPassword[BUFFLEN];

    if(file){
        while(fscanf(file, "%s %s", readUser, readPassword) != EOF){
            if(!strcmp(readUser, user)){
                //user registered
                return 1;
            }
        }
    }
    //user not registered
    return 0;
}

char* trim(char *text){
    int index = 0;
    while(text[index] == ' ' || text[index] == '\t'){
        index++;
    }
    char *temp = strchr(text,text[index]);
    return temp;
}

void append_to_file(char fpath[BUFFLEN], char content[BUFFLEN]){
    FILE* file = fopen(fpath, "a");
    fprintf(file, "%s\n", content);
    fclose(file);
}

int check_data_type(char* buffer){
    char bufferCopy[BUFFLEN];
    strcpy(bufferCopy, buffer);
    char singleQuote = '\'';

    if(bufferCopy[0] == singleQuote && bufferCopy[strlen(bufferCopy)-1] == singleQuote){//string begins with ' ends with '
        return 1;
    }
    for(int i = 0; i < strlen(bufferCopy); i++){
        if(bufferCopy[i] < '0' || bufferCopy[i] > '9'){
            return 0; //data type error
        }
    }
    return 2; //integer
}

void removeLastCharacter(char* str) {
    int length = strlen(str);
    if (length > 0) {
        str[length - 1] = '\0';
    }
}

int login(char* user_type, char* username){
   
    FILE *file = fopen(users_list, "r");
    char user[BUFFLEN];
    char pass[BUFFLEN];

    char* token = strtok(user_type, " ");
    strcpy(user, token);
    token = strtok(NULL, " ");
    strcpy(pass, token);

    char readUser[BUFFLEN];
    char readPassword[BUFFLEN];
    if(file){
        while(fscanf(file, "%s %s", readUser, readPassword) != EOF){
            if(strcmp(readUser, user) == 0 && strcmp(readPassword, pass) == 0){ //username and password exist
                fclose(file);
                strcpy(username, user);
                return 1; //login succeed
            }
        }
        fclose(file);
    }
    return 0; //login failed, username and password does not match or not exist
}

int create_user(char* buffer, char* user_type){

    if(strcmp(user_type, "root") != 0){
        //only root may create user
        return 0;
    }

    size_t len = strlen(buffer);
    if (len > 0){
        char semicolon = buffer[len-2];
        if (semicolon != ';'){
            return -3;
        }
    }

    char bufferCopy[BUFFLEN] = {0};
    strcpy(bufferCopy, buffer);
   
    char* token = strtok(bufferCopy, ";");
    token = strtok(bufferCopy, " "); //CREATE
    char input[10][BUFFLEN];
    int i = 0;

    while (token != NULL) { //push word by word to an array of string
        strcpy(input[i],token);
        i++;
        token = strtok(NULL, " ");
    }

    // CREATE USER username IDENTIFIED BY password
    //    0    1      2         3      4     5
    if(strcmp("IDENTIFIED", input[3]) || strcmp("BY", input[4])){
        return -1; //syntax error
    }

    if (i < 6){
        return -1;
    }

    if(!is_user_exist(input[2])){ //if user not exist, create user
        char new_user[BUFFLEN];
        sprintf(new_user, "%s %s", input[2], input[5]);
        append_to_file(users_list, new_user);
        return 1; //user created
    }else {
        return -2; //user already registered
    }
}

int grant_pemission(char* buffer, char* user_type){

    size_t len = strlen(buffer);
    if (len > 0){
        char semicolon = buffer[len-2];
        if (semicolon != ';'){
            return -4;
        }
    }

    if(strcmp(user_type, "root")){
        return 0; //only root may grant permission
    }
    
    char bufferCopy[BUFFLEN] = {0};
    strcpy(bufferCopy, buffer);
   
    char* token = strtok(bufferCopy, ";");
    token = strtok(bufferCopy, " ");
    char input[10][BUFFLEN];

    int i = 0;
    while (token != NULL) { //put words in array
        strcpy(input[i], token);
        i++;
        token = strtok(NULL, " ");
    }

    if(i < 5 || strcmp("INTO", input[3])){
        return -1; //syntax error
    }

    // GRANT PERMISSION dbName INTO user
    //   0        1       2     3    4
    char grant[BUFFLEN] = {0};
    if(is_db_exist(input[2])){
        if(is_user_exist(input[4])){
            sprintf(grant, "%s %s", input[2], input[4]);
            append_to_file(database_and_permission_list,grant);
            return 1; //granted
        }else{
            return -3; //user not exist
        }
    }else{
        return -2; //database not exist
    }
}

int create_database(char* buffer, char* user_type, char* username){

    size_t len = strlen(buffer);
    if (len > 0){
        char semicolon = buffer[len-2];
        if (semicolon != ';'){
            return -2;
        }
    }

    char bufferCopy[BUFFLEN] = {0};
    char databaseName[BUFFLEN] = {0};
    strcpy(bufferCopy, buffer);
    
    char* token = strtok(bufferCopy, ";");
    token = strtok(bufferCopy, " "); //CREATE
    token = strtok(NULL, " "); //DATABASE
    token = strtok(NULL, " "); //dbName

    if(token != NULL){
        strcpy(databaseName, token);
    }else{
        return -1; //syntax error, expected database name
    }

    char fpath[BUFFLEN] = {0};
    sprintf(fpath, "%s/databases/%s", workspace, databaseName);
    
    if(mkdir(fpath, 0777) == 0){//database creation succeed
        char new_db[BUFFLEN] = {0};
        sprintf(new_db, "%s %s", databaseName, username);
        append_to_file(database_and_permission_list, new_db); //add db name and creator to list
        return 1;
    }else{
        return 0; //database already existed
    }
}

int use(char* buffer, char* user_type, char* username, char* use_database){

    size_t len = strlen(buffer);
    if (len > 0){
        char semicolon = buffer[len-2];
        if (semicolon != ';'){
            return -3;
        }
    }

    char bufferCopy[BUFFLEN] = {0};
    char databaseName[BUFFLEN] = {0};
    strcpy(bufferCopy, buffer);
    
    char* token = strtok(bufferCopy, ";");
    token = strtok(bufferCopy," "); //USE
    token = strtok(NULL, " "); //dbName
    
    if(token != NULL){
        strcpy(databaseName, token);
    }else{
        return -1; //syntax error, expected database name
    }

    FILE* file = fopen(database_and_permission_list, "r");

    char readDB[BUFFLEN];
    char readUser[BUFFLEN];
    bool db_exist = false;

    if(file){
        while(fscanf(file, "%s %s", readDB, readUser) != EOF){
            if(strcmp(readDB, databaseName) == 0){
                db_exist = true;
                if(strcmp(readUser, username) == 0 || strncmp(user_type, "root", 4) == 0){
                    fclose(file);
                    strcpy(use_database, databaseName);
                    return 1; //use database
                }
            }
        }
    }
    if(!db_exist){
        return -2; //database not exist
    }
    return 0;
}

//char* tipe is not used at all
int drop_database(char* buffer, char* tipe, char* username, char* use_database){

    size_t len = strlen(buffer);
    if (len > 0){
        char semicolon = buffer[len-2];
        if (semicolon != ';'){
            return -3;
        }
    }

    char bufferCopy[BUFFLEN] = {0};
    strcpy(bufferCopy, buffer);
    
    char* token = strtok(bufferCopy, ";");
    token = strtok(bufferCopy, " "); //DROP
    token = strtok(NULL, " "); //DATABASE
    token = strtok(NULL, " "); //dbName
    
    char database[BUFFLEN] = {0};
    if(token != NULL){
        strcpy(database, token);
    }else{
        return -1; //syntax error, expected database name
    }

    char readDB[BUFFLEN];
    char readUser[BUFFLEN];
    bool is_db_exist = false;
    bool is_user_has_access =false;

    FILE* file = fopen(database_and_permission_list, "r");

    if(file){
        while(fscanf(file, "%s %s", readDB, readUser) != EOF){
            if(strcmp(readDB, database) == 0){
                is_db_exist = true;
                if(strcmp(readUser, username) == 0){
                    fclose(file);
                    is_user_has_access = true;
                    break;
                }
            }
        }

        if(is_user_has_access || (is_db_exist && strcmp(username, "root") == 0)){
            //deleting database
            char fpath[BUFFLEN] = {0};
            sprintf(fpath, "%s/databases/%s", workspace, database);
            char command[BUFFLEN];
            sprintf(command, "rm -rf %s", fpath);
            system(command);
            
            FILE* file_read = fopen(database_and_permission_list, "r");
            char new_database_and_permission_list[BUFFLEN] = {0};
            sprintf(new_database_and_permission_list, "%s/databases/administrator/temp.txt", workspace);

            FILE* file_write = fopen(new_database_and_permission_list, "w");
            while(fscanf(file_read, "%s %s", readDB, readUser) != EOF){
                if(strcmp(readDB, database)){ //if dbName != deleted-database name, append
                    char record[BUFFLEN] = {0};
                    fprintf(file_write, "%s %s\n", readDB, readUser);
                }
            }

            fclose(file_write);
            remove(database_and_permission_list);
            rename(new_database_and_permission_list, database_and_permission_list);

            if(strcmp(use_database, database) == 0){
                memset(use_database, 0, sizeof(use_database));
            }
            return 1; //drop database succeed
        }        
    }

    if(!is_db_exist){
        return -2; //database does not exist
    }

    return 0;
}

int create_table(char* buffer, char* use_database){

    size_t len = strlen(buffer);
    if (len > 0){
        char semicolon = buffer[len-2];
        if (semicolon != ';'){
            return -5;
        }
    }

    if(strlen(use_database) == 0){
        return -2;
    }

    char bufferCopy[BUFFLEN] = {0};
    char tableName[BUFFLEN] = {0};
    strcpy(bufferCopy, buffer);
    
    char* token = strtok(bufferCopy, ";");
    token = strtok(bufferCopy, " "); //CREATE
    token = strtok(NULL, " "); //TABLE
    token = strtok(NULL, " "); //tableName

    if(token != NULL){
        strcpy(tableName, token);
    }else{
        return -1; //syntax error, expected table name
    }

    char* temp;
    temp = strchr(buffer, '('); // (col1 string, col2 int);
    if(!temp){
        return -1; //missing left parentheses
    }else{
        temp = temp + 1; //shift to character after '('
    }

    char attributes[BUFFLEN] = {0};
    strcpy(attributes, temp);

    char* token2 = strtok(attributes, ";"); // col1 string, col2 int)
    token2 = strtok(attributes, ")"); // col1 string, col2 int

    char attr_array[100][BUFFLEN] = {0};
    int attr_count = 0;
    token2 = strtok(attributes, ",");

    while(token2 != NULL){
        strcpy(attr_array[attr_count], trim(token2));
        attr_count++;
        token2 = strtok(NULL, ",");
    }

    for(int i = 0; i < attr_count; i++){
        char temp2[BUFFLEN] = {0}; 
        char attr_name[BUFFLEN] = {0};
        char data_type[BUFFLEN] = {0};

        strcpy(temp2, attr_array[i]);
        char* token3 = strtok(temp2, " "); // col1
        strcpy(attr_name, token3);
        token3 = strtok(NULL, " "); // string or int

        if(token3){
            strcpy(data_type, token3);
            if(strcmp(data_type, "int") != 0 && strcmp(data_type, "string") != 0){
                return -4; //data type not allowed, only string or int allowed
            }
        }else{
            return -3; //syntax error, incomplete attribute declaration
        }
    }

    char fpath[BUFFLEN] = {0};
    sprintf(fpath, "%s/databases/%s/%s.txt", workspace, use_database, tableName);

    FILE* file = fopen(fpath, "r"); //attempt to open read table
    if(!file){
        file = fopen(fpath, "w");
        char attr_list_path[BUFFLEN] = {0};
        sprintf(attr_list_path, "%s/databases/%s/%s-attributes.txt", workspace, use_database, tableName);

        for(int i = 0; i < attr_count; i++){
            append_to_file(attr_list_path, attr_array[i]);
        }
        return 1; //table creation succeed
    }else{
        return 0; //open read succeed, table existed, cannot create table with same name
    }
}

int drop_table(char* buffer, char* use_database){

    size_t len = strlen(buffer);
    if (len > 0){
        char semicolon = buffer[len-2];
        if (semicolon != ';'){
            return -3;
        }
    }

    if(!strlen(use_database)){
        return -2; //syntax error expected database name
    }

    char bufferCopy[BUFFLEN];
    strcpy(bufferCopy, buffer);
    char* token = strtok(bufferCopy, ";"); //DROP TABLE tableName
    token = strtok(token, " "); //DROP
    token = strtok(NULL, " "); //TABLE
    token = strtok(NULL, " "); //tableName

    char table_attr_path[BUFFLEN] = {0};
    sprintf(table_attr_path, "%s/databases/%s/%s-attributes.txt", workspace, use_database, token);

    char table_path[BUFFLEN]={0};
    sprintf(table_path, "%s/databases/%s/%s.txt", workspace, use_database, token);

    FILE* file;
    file = fopen(table_attr_path, "r"); //attempting to open read table_attr to check if its exist
    if(file){
        fclose(file);
        remove(table_attr_path);
        remove(table_path);
        return 1; //drop table succeed
    }
    else{
        return -1; //table does not exist
    }
}

int drop_column(char* buffer, char* use_database){

    size_t len = strlen(buffer);
    if (len > 0){
        char semicolon = buffer[len-2];
        if (semicolon != ';'){
            return -4;
        }
    }

    if(!strlen(use_database)){
        return -2;
    }

    char input[10][BUFFLEN];
    char bufferCopy[BUFFLEN];
    strcpy(bufferCopy, buffer);

    int k = 0;
    char* token = strtok(bufferCopy, ";"); //DROP COLLUMN colName FROM tableName
    token = strtok(token, " "); //DROP
    while (token != NULL) {
        strcpy(input[k], token);
        k++;
        token = strtok(NULL, " ");
    }
    // DROP COLLUMN colName FROM tableName
    //  0      1       2     3       4

    if(k != 5){
        return -3; //not enough argument or syntax error
    }

    FILE *strukturin, *strukturout, *tablein, *tableout;

    char open[BUFFLEN]={0}, append[BUFFLEN]={0}, a[BUFFLEN]={0}, b[BUFFLEN]={0};
    sprintf(open, "%s/databases/%s/%s-attributes", workspace, use_database, input[4]);
    strcpy(a,open);
    strcat(a,"2.txt");
    strcat(open,".txt");

    strukturin = fopen(open,"r");
   
    int i = 0,j = 0;
    char data_type[1000], name[1000];

    if(strukturin){
        strukturout = fopen(a,"w");
        while(fscanf(strukturin,"%s %s",name,data_type) != EOF){
            if(strcmp(input[2],name)){
                fprintf(strukturout,"%s %s\n",name,data_type);
            }
            else i = j;
            j++;
        }
        fclose(strukturin);
        fclose(strukturout);
    }
    else{
        return -1;
    }

    sprintf(append,"%s/databases/%s/%s",workspace,use_database,input[4]);
    strcpy(b,append);
    strcat(b,"2.txt");
    strcat(append,".txt");

    tablein = fopen(append,"r");
    tableout = fopen(b,"w");

    char ambil[1000];
    while(fgets(ambil,1000,tablein)){
        token = strtok(ambil,",");
        int j = 0;
        char baru[1000];
        strcpy(baru,"");
        while(token!=NULL){
            if(j!=i){
                strcat(baru,token);
                strcat(baru,",");
            }
            token = strtok(NULL,",");
            j++;
        }
        baru[strlen(baru)-1] = 0;
        fprintf(tableout,"%s",baru);
        if(baru[strlen(baru)-1] != '\n'){
            fprintf(tableout,"\n");
        }
    }
    fclose(tablein);
    fclose(tableout);
    remove(open);
    rename(a,open);
    remove(append);
    rename(b,append);
    return 1;
}
//-------------------------------------------------------------------------------------

int insert(char* buffer, char* use_database){

    size_t len = strlen(buffer);
    if (len > 0){
        char semicolon = buffer[len-2];
        if (semicolon != ';'){
            return -5;
        }
    }

    if(!strlen(use_database)){
        return -1; //syntax error, expected database name
    }

    char bufferCopy[BUFFLEN];
    strcpy(bufferCopy, buffer);

    char *token = strchr(bufferCopy, '('); // (val1, val2)
    token = token + 1; // val1, val2)
    token = strtok(token, ")"); // val1, val2
    token = strtok(token, ","); // val1

    char data[100][BUFFLEN];

    int i = 0;

    while(token!=NULL){
        strcpy(data[i], trim(token));
        i++;
        token = strtok(NULL, ",");
    }

    FILE* fileRead;

    strcpy(bufferCopy, buffer);
    token = strtok(bufferCopy," "); //INSERT
    token = strtok(NULL, " "); //INTO
    token = strtok(NULL, " "); //tableName

    char fpath[BUFFLEN] = {0};
    sprintf(fpath, "%s/databases/%s/%s-attributes.txt", workspace, use_database, token);
    fileRead = fopen(fpath, "r");
   
    char data_type[100][BUFFLEN];
    char readDataType[BUFFLEN];
    char value[BUFFLEN];
    int k = 0;

    if(fileRead){
        while(fscanf(fileRead, "%s %s", value, readDataType) != EOF){
            strcpy(data_type[k], readDataType);
            k++;
        }
    }
    else{
        return -2; //table does not exist
    }

    FILE *fileWrite;
    char tablePath[BUFFLEN] = {0};
    sprintf(tablePath, "%s/databases/%s/%s.txt", workspace, use_database, token);
    fileWrite = fopen(tablePath, "a");

    if(k != i){
        fclose(fileRead);
        fclose(fileWrite);
        return -3; //not enough argument passed
    }

    for(int j = 0; j < i; j++){
        int val = check_data_type(data[j]);
        if(val == 1 && !strcmp(data_type[j], "string"));
        else if (val == 2 && !strcmp(data_type[j], "int"));
        else {
            fclose(fileRead);
            fclose(fileWrite);
            return -4; //data type error
        }  
    }
    for(int j = 0; j < i-1; j++){
        fprintf(fileWrite, "%s,", data[j]); //inserting data, comma separated
    }
    fprintf(fileWrite,"%s\n", data[i-1]);
    fclose(fileRead);
    fclose(fileWrite);

    return 1; //insertion succeed
}

int delete_from(char* buffer, char* use_database){

    size_t len = strlen(buffer);
    if (len > 0){
        char semicolon = buffer[len-2];
        if (semicolon != ';'){
            return -5;
        }
    }

    if(!strlen(use_database)){
        return -1;
    }

    char bufferCopy[BUFFLEN] = {0};
    strcpy(bufferCopy, buffer);
   
    char* token = strtok(bufferCopy, ";"); //DELETE FROM tableName
    token = strtok(bufferCopy," "); //DELETE
    char input[10][BUFFLEN];
    int i = 0;

    while (token != NULL) {
        strcpy(input[i], token);
        i++;
        token = strtok(NULL, " ");
    }
    //DELETE FROM tableName
    //   0     1      2
    if(i == 3){
        //no where statement, clear all records
        char fpath[BUFFLEN];
        sprintf(fpath, "%s/databases/%s/%s.txt", workspace, use_database, input[2]);
        FILE* file = fopen(fpath, "r");
        if(file){
            fclose(file);
            file = fopen(fpath, "w"); //using write on existed file effectively rewrite all records
            fprintf(file, "");
            fclose(file);
        }
        else{
            return -3; //open read failed, table does not exist
        }
        return 1; //delete succeed
    
    //DELETE FROM tableName WHERE colName=Value
    //  0     1       2       3         4
    }else if(i == 5){
        if(strcmp(input[3], "WHERE")){
            return -2; //syntax error, missing WHERE statement
        }

        char whereCol[BUFFLEN];
        char whereVal[BUFFLEN];

        token = strtok(input[4], "="); //colname
        strcpy(whereCol, token);
        token = strtok(NULL, "="); //value
        strcpy(whereVal, token);

        char attr_path[BUFFLEN];
        sprintf(attr_path, "%s/databases/%s/%s-attributes.txt", workspace, use_database, input[2]);
        FILE* file = fopen(attr_path, "r");

        int where_col_idx = -1;
        if(file){
            int count = 0;
            char readCol[BUFFLEN];
            char readDataType[BUFFLEN];
            while(fscanf(file, "%s %s", readCol, readDataType) != EOF){
                if(strcmp(whereCol, readCol) == 0){
                    where_col_idx = count;
                }
                count++;
            }

            if(where_col_idx == -1){
                fclose(file);
                return -4; //no colName matches whereColName
            }
            fclose(file);
        }else{
            return -3; //table does not exist
        }

        char fpath[BUFFLEN];
        char new_fpath[BUFFLEN];

        sprintf(fpath, "%s/databases/%s/%s.txt", workspace, use_database, input[2]);
        sprintf(new_fpath, "%s/databases/%s/%s_copy.txt", workspace, use_database, input[2]);

        FILE* file_read = fopen(fpath, "r");
        if(file_read){
            char record[BUFFLEN];
            FILE* file_write = fopen(new_fpath, "w");

            while(fgets(record, BUFFLEN, file_read)){
                char recordCopy[BUFFLEN];
                strcpy(recordCopy, record);
                removeLastCharacter(recordCopy); //remove \n 

                token = strtok(recordCopy, ","); // val1
                char new_record[BUFFLEN] = "";
                bool is_deleted = false;

                int curCol = 0;
                while(token != NULL){
                    strcat(new_record, token);
                    strcat(new_record, ",");

                    if(curCol == where_col_idx){
                        if(strcmp(whereVal, token) == 0){
                            is_deleted = true;
                        }
                    }
                    token = strtok(NULL, ","); //nextVal
                    curCol++;
                }

                if(!is_deleted){ //if not deleted append to file
                    removeLastCharacter(new_record); //remove tailing comma
                    strcat(new_record, "\n");
                    fprintf(file_write, "%s", new_record);
                }
            }
            fclose(file_read);
            fclose(file_write);
            remove(fpath);
            rename(new_fpath, fpath);
            return 1; //deletion succeed

        }else{
            return -3; //table does not exist
        }

    }else{
        return -2; //syntax error, argument does not match
    }
}

int update(char* buffer, char* use_database){

    size_t len = strlen(buffer);
    if (len > 0){
        char semicolon = buffer[len-2];
        if (semicolon != ';'){
            return -5;
        }
    }

    if(!strlen(use_database)){
        return -1;
    }

    char bufferCopy[BUFFLEN] = {0};
    strcpy(bufferCopy, buffer);

    char* token = strtok(bufferCopy, ";"); //UPDATE tableName SET colName = value
    token = strtok(bufferCopy, " "); //UPDATE
    char input[10][BUFFLEN];
    int i = 0;

    while (token != NULL) {
        strcpy(input[i], token);
        i++;
        token = strtok(NULL, " ");
    }

    //UPDATE tableName SET colName=value
    //  0        1      2        3
    if(strcmp(input[2], "SET")){
        return -2; //syntax error
    }

    char colName[BUFFLEN];
    char newVal[BUFFLEN];

    token = strtok(input[3], "="); //colName
    strcpy(colName, token);
    token = strtok(NULL, "="); //value
    strcpy(newVal, token);

    char attr_path[BUFFLEN];
    sprintf(attr_path, "%s/databases/%s/%s-attributes.txt", workspace, use_database, input[1]);
    FILE *file = fopen(attr_path, "r");

    int col_idx = -1;
    int count = 0;
    char readDataType[BUFFLEN];
    char readColName[BUFFLEN];
    if(file){
        while(fscanf(file, "%s %s", readColName, readDataType) != EOF){
            if(strcmp(colName, readColName) == 0){
                col_idx = count;
            }
            count++;
        }
        if(col_idx == -1){
            fclose(file);
            return -4; //attribute does not exist
        }
        fclose(file);
    
    }else{
        return -3; //table does not exist
    }

    //WHERE statement
    int where_col_idx = -1;
    char whereVal[BUFFLEN];

    //UPDATE tableName SET colName=value WHERE colName=value
    //  0        1      2         3        4         5
    if(input[4] != NULL && strcmp(input[4], "WHERE") == 0){ //execute if input[4] = WHERE
        char whereColName[BUFFLEN];
        token = strtok(input[5], "="); //colName
        strcpy(whereColName, token);
        token = strtok(NULL, "="); //value
        strcpy(whereVal, token);
        
        FILE* fileWhere = fopen(attr_path, "r");
        char readWhereColName[BUFFLEN];
        char readWhereDataType[BUFFLEN];
        int whereCount = 0;
        while(fscanf(fileWhere, "%s %s", readWhereColName, readWhereDataType) != EOF){
            if(strcmp(whereColName, readWhereColName) == 0){
                where_col_idx = whereCount;
            }
            whereCount++;
        }
        if(where_col_idx == -1){
            fclose(fileWhere);
            return -4; //where attribute not exist
        }
        fclose(fileWhere);
    }

    char fpath[BUFFLEN];
    char new_fpath[BUFFLEN];
    sprintf(fpath, "%s/databases/%s/%s.txt", workspace, use_database, input[1]);
    sprintf(new_fpath, "%s/databases/%s/%s_copy.txt", workspace, use_database, input[1]);

    FILE* file_read;
    FILE* file_write;
    file_read = fopen(fpath, "r");
    file_write = fopen(new_fpath, "w");

    if(file_read){
        char record[BUFFLEN];

        while(fgets(record, BUFFLEN, file_read) != NULL){
            
            char recordCopy[BUFFLEN];
            strcpy(recordCopy, record);
            removeLastCharacter(recordCopy);

            token = strtok(recordCopy, ","); // val1
            int curCol = 0;
            char new_record[BUFFLEN] = "";
            bool is_where = false;

            while(token != NULL){
                if(curCol == where_col_idx){
                    if(strcmp(whereVal, token) == 0){
                        is_where = true; //where found
                    }
                }
                //creating new record
                if(curCol == col_idx){
                    strcat(new_record, newVal); //updated value in said query
                    strcat(new_record, ",");
                }else{
                    strcat(new_record, token); //everything else
                    strcat(new_record, ",");
                }
                token = strtok(NULL, ","); //next val
                curCol++;
            }
            //if no update statement -> all record modified
            //if there is update statement and current record match the where statement -> append new record
            if(is_where == true || where_col_idx == -1){
                removeLastCharacter(new_record); //replace tailing comma with null to end record
                strcat(new_record, "\n");
                fprintf(file_write, "%s", new_record);

            //else append original record
            }else{
                fprintf(file_write, "%s", record);
            }
        }
        fclose(file_read);
        fclose(file_write);
        remove(fpath);
        rename(new_fpath, fpath);
        return 1; //update succeed
    }
}

int select_table(char* buffer, char* use_database, int* new_socket){
    if(!strlen(use_database)){
        return -1;
    }

    size_t len = strlen(buffer);
    if (len > 0){
        char semicolon = buffer[len-2];
        if (semicolon != ';'){
            return -5;
        }
    }
    
    char bufferCopy[BUFFLEN] = {0};
    strcpy(bufferCopy, buffer);
   
    char* token = strtok(bufferCopy, ";"); //SELECT colNames FROM tableName
    token = strtok(bufferCopy, " "); //SELECT
    char input[10][BUFFLEN] = {0};
    int argc = 0;

    while (token != NULL) {
        strcpy(input[argc], token);
        argc++;
        token = strtok(NULL, " ");
    }

    bool star = false;
    int where_col_idx = -1;
    int FROM_idx;
    int selected_cols[20];
    char whereCol[BUFFLEN];
    char whereVal[BUFFLEN];
    char attr_path[BUFFLEN];

    FILE* read_attr;

    //star
    //SELECT * FROM tableName
    //  0    1  2       3
    if(strcmp(input[1], "*") == 0){
        sprintf(attr_path, "%s/databases/%s/%s-attributes.txt", workspace, use_database, input[3]);

        FILE* read_attr = fopen(attr_path, "r");
        if(!read_attr){
            return -3; //table does not exist
        }
        fclose(read_attr); //table exist, continue

        if(strcmp(input[2], "FROM")){
            return -2; //syntax error, missing FROM after *
        }

        star = true;
        FROM_idx = 2;

        //SELECT * FROM tableName WHERE colName=value
        //  0    1   2      3       4         5
        if(input[4] != NULL && strcmp(input[4], "WHERE") == 0){

            token = strtok(input[5], "="); //whereCol
            strcpy(whereCol, token);
            token = strtok(NULL, "="); //whereVal
            strcpy(whereVal, token);

            if(strcmp(input[4], "WHERE") == 0){
                read_attr = fopen(attr_path, "r");
                char readCol[BUFFLEN];
                char readDataType[BUFFLEN];
                int k = 0;
                while(fscanf(read_attr, "%s %s", readCol, readDataType) != EOF){
                    if(strcmp(whereCol, readCol) == 0){
                        where_col_idx = k; //retrieve where_col_idx
                    }
                    k++;
                }
                if(where_col_idx == -1){
                    fclose(read_attr); 
                    return -4; //whereCol not exist
                }
                fclose(read_attr);
            }
        }
    
    //colls
    //SELECT col1, ..., coln FROM tableName
    //  0     1           n   n+1    n+2
    }else{
        int cur_idx = 0;
        while(cur_idx < argc && strcmp(input[cur_idx], "FROM")){
            cur_idx++; //getting idx of FROM statement, end of colNames
        }
        FROM_idx = cur_idx;

        if(FROM_idx == argc){
            return -2; //syntax error, missing FROM statement or table name
        }

        int tableName_idx = FROM_idx + 1;
        sprintf(attr_path, "%s/databases/%s/%s-attributes.txt", workspace, use_database, input[tableName_idx]);
        read_attr = fopen(attr_path, "r");
        if(!read_attr){
            return -3; //table does not exist
        }
        fclose(read_attr);
        
        for(int x = 0; x < 20; x++){
            selected_cols[x] = -1;
        }

        cur_idx = 1; //after SELECT
        int col_count = 0;
        int col_found = 0;
        while(cur_idx < FROM_idx){
            col_count++;
            read_attr = fopen(attr_path, "r");
            char readCol[BUFFLEN];
            char readDataType[BUFFLEN]; //the input was tokenized by space
            char readColWithComma[BUFFLEN]; //so if the selected col is more than 1 then some of them still contains comma

            int attr_idx = 0;
            while(fscanf(read_attr, "%s %s", readCol, readDataType) != EOF){
                strcpy(readColWithComma, readCol);
                strcat(readColWithComma, ",");

                if(strcmp(input[cur_idx], readCol) == 0 || strcmp(input[cur_idx], readColWithComma) == 0){
                    selected_cols[col_found] = attr_idx;
                    col_found++;
                }
                attr_idx++;
            }

            if(col_count != col_found){ //if after searching col_found is not incremented means there is a col not found, quit
                fclose(read_attr);
                return -4; //col does not exist
            }
            fclose(read_attr);
            cur_idx++;
        }
        
        //SELECT col1, ..., coln FROM tableName WHERE col=Value
        //  0     1          n    n+1    n+2     n+3     n+4
        int WHERE_idx = FROM_idx + 2;
        if(strcmp(input[WHERE_idx], "WHERE") == 0 && input[WHERE_idx + 1] != NULL){
            token = strtok(input[WHERE_idx + 1], "="); //whereColName
            strcpy(whereCol, token);
            token = strtok(NULL, "="); //whereColVal
            strcpy(whereVal, token);

            read_attr = fopen(attr_path, "r");
            char readCol[BUFFLEN];
            char readDataType[BUFFLEN];
            int attr_idx = 0;
            while(fscanf(read_attr, "%s %s", readCol, readDataType) != EOF){
                if(strcmp(whereCol, readCol) == 0){
                    where_col_idx = attr_idx;
                }
                attr_idx++;
            }

            if(where_col_idx == -1){
                fclose(read_attr);
                return -4; //where col does not exist
            }
            fclose(read_attr);
        }
    }

    char path[BUFFLEN];
    sprintf(path, "%s/databases/%s/%s.txt", workspace, use_database, input[FROM_idx+1]);
    
    FILE* file_read = fopen(path, "r");
    char record[BUFFLEN];

    if(file_read){
        while(fgets(record, BUFFLEN, file_read)){
            removeLastCharacter(record); //delete \n

            if(star){
                if(where_col_idx == -1){ //star no WHERE, print all
                    char msg[BUFFLEN];
                    sprintf(msg, "%s\n", record);
                    send(*new_socket , msg , strlen(msg) , 0 );

                }else{ //star + WHERE
                    char readCopy[BUFFLEN];
                    strcpy(readCopy, record);
                    token = strtok(readCopy, ",");
                    int curCol = 0;
                    while(token != NULL){
                        if(curCol == where_col_idx){
                            char whereValWithEndl[BUFFLEN];
                            sprintf(whereValWithEndl, "%s\n", whereVal);

                            if(strcmp(whereVal, token) == 0 || strcmp(whereValWithEndl, token) == 0){
                                char msg[BUFFLEN];
                                sprintf(msg, "%s\n", record);
                                send(*new_socket, msg , strlen(msg), 0 );
                                break;
                            }
                        }
                        token = strtok(NULL, ",");
                        curCol++;
                    }
                }

            }else{ //spesific column
                if (where_col_idx == -1){ //no WHERE
                    char msg[BUFFLEN] = "";
                    int idx = 0;
                    while (selected_cols[idx] != -1){
                        char readCopy[BUFFLEN];
                        strcpy(readCopy, record);
                        token = strtok(readCopy, ",");
                        int curCol = 0;
                        while(token != NULL){
                            if(curCol == selected_cols[idx]){
                                strcat(msg, token);
                                strcat(msg, ",");
                                break;
                            }
                            token = strtok(NULL, ",");
                            curCol++;
                        }                   
                        idx++;
                    }
                    removeLastCharacter(msg); //replace tailing comma with null
                    strcat(msg, "\n");
                    send(*new_socket , msg , strlen(msg) , 0 );

                }else{
                    //with WHERE
                    char msg[BUFFLEN] = "";
                    int idx = 0;
                    bool qualified = false;
                    while (selected_cols[idx] != -1){
                        char readCopy[BUFFLEN];
                        strcpy(readCopy, record);
                        token = strtok(readCopy, ",");
                        int curCol = 0;
                        while(token != NULL){
                            if(curCol == selected_cols[idx]){
                                strcat(msg, token);
                                strcat(msg, ",");
                                break;
                            }
                            token = strtok(NULL, ",");
                            curCol++;
                        }                   
                        idx++;
                    }
                    removeLastCharacter(msg); //replace tailing comma with null
                    strcat(msg, "\n");
                    send(*new_socket , msg , strlen(msg) , 0 );
                }              
            }
        }
        return 1;
    }
    else{
        return -2;
    }
}

//-------------------------------------------------------------------------------------
void dump_commands(int *new_socket, char *use_database){
    DIR* dir;
    struct dirent* ent;
    char fpath[BUFFLEN] = {0};
    sprintf(fpath,"%s/databases/%s",workspace,use_database);

    dir = opendir(fpath);

    if (dir != NULL){
        while ((ent = readdir(dir)) != NULL) {
          if(strstr(ent->d_name, "attributes") != NULL){
              char table_name[BUFFLEN] = {0};
              char temp[BUFFLEN] = {0};
              strcpy(temp, ent->d_name);
              char* token = strtok(temp, "-");
              strcpy(table_name, token);

              FILE* file;
              char path[BUFFLEN] = {0};
              sprintf(path, "%s/databases/%s/%s", workspace, use_database, table_name);

              char kolom[100][BUFFLEN] = {0};
              int jumlah_kolom = 0;
              file = fopen(path, "r");
              if(file){
                char temp2[BUFFLEN];
                while((fscanf(file,"%[^\n]%*c",temp2)) != EOF){
                    strcpy(kolom[jumlah_kolom++],temp2);
                }
                fclose(file);

                int valread;
                char command[BUFFLEN] = {0}, buffer[BUFFLEN] = {0}, table[BUFFLEN] = {0};
                
                strcpy(table, table_name);
                char *token_table = strtok(table,".");
                sprintf(command,"DROP TABLE %s;",token_table);
                send(*new_socket , command , strlen(command) , 0 );
                valread = recv( *new_socket , buffer, 1024, 0);
            
                bzero(command,sizeof(command));
                sprintf(command,"CREATE TABLE %s (",token_table);
                for(int i = 0; i < jumlah_kolom; i++){
                    strcat(command,kolom[i]);
                    strcat(command,",");
                }
                command[strlen(command)-1] = ')';
                strcat(command,";");

                send(*new_socket , command , strlen(command) , 0 );
                valread = recv( *new_socket , buffer, 1024, 0);

                bzero(path,sizeof(path));
                sprintf(path,"%s/databases/%s/%s",workspace,use_database,table_name);

                file = fopen(path,"r");
                if(file){
                    while((fscanf(file,"%[^\n]%*c",temp2)) != EOF){
                        bzero(command,sizeof(command));
                        sprintf(command,"INSERT INTO %s VALUES (%s);",token_table,temp2);
                        send(*new_socket , command , strlen(command) , 0 );
                        valread = recv( *new_socket , buffer, 1024, 0);
                    }
                    fclose(file);
                }
                
            }
        }
    }
    char command[1000] = {0};
    bzero(command,sizeof(command));
    strcpy(command,"DONE!!!");
    send(*new_socket , command , strlen(command) , 0 );

      (void) closedir (dir);
    } else perror ("Couldn't open the directory");
}
//---------------------------------------------------------------------------------------------

void append_log(char *login_user, char *command){
    FILE *file;
    char fpath[BUFFLEN] = {0};
    sprintf(fpath, "%s/dblog.log", workspace);
    file = fopen(fpath, "a");

    char time_now[100] = {0};
	time_t t = time(NULL);
	struct tm *tm = localtime(&t);
	strftime(time_now,100,"%Y-%m-%d %H:%M:%S",tm);
    fprintf(file, "%s:%s:%s\n", time_now, login_user, command);
	fclose(file);
}

void* play(void* arg){
    bool mode_dump = false;
    int* new_socket = (int *) arg;
    int valread;
    char tipe[BUFFLEN] = {0};
    char tmptipe[BUFFLEN] = {0};
    valread = recv( *new_socket , tipe, BUFFLEN, 0);
    char login_user[100] = {0};
    char use_database[100] = {0};

    strcpy(tmptipe, tipe);

    if(!strncmp(tipe,"root",4)){
        strcpy(login_user,"root");
        char status_login[1000] = {0};
        strcpy(status_login,"authentication success");
        send(*new_socket , status_login , strlen(status_login) , 0 );

        char *ret = strstr(tmptipe,"dump");
        if(ret){
            mode_dump = true;
        }

    }else{
        //cek login
        int masuk = login(tipe,login_user);
        char status_login[1000] = {0};
        
        if(masuk == 0){
            strcpy(status_login,"authentication failed");
            send(*new_socket , status_login , strlen(status_login) , 0 );
            printf("login gagal\n");
            close(*new_socket);
            return NULL;
        }
        strcpy(status_login,"authentication success");
        send(*new_socket , status_login , strlen(status_login) , 0 );

        printf("berhasil login %s\n",login_user);

        char *ret = strstr(tmptipe,"dump");
        if(ret){
            mode_dump = true;
        }
    }

    if(mode_dump){
        char buffer[BUFFLEN] = {0};
        char message[BUFFLEN] = {0};
        valread = recv( *new_socket , buffer, BUFFLEN, 0);
        
        if(!strncmp(buffer, "USE", 3)){
            append_log(login_user, buffer);
            int status = use(buffer,tipe,login_user,use_database);
            if(status == -2){
                strcpy(message,"unknown database");
                close(*new_socket);
            }else if(status == -1){
                strcpy(message,"syntax error");
                close(*new_socket);
            }else if(status == 1){
                sprintf(message,"database changed to %s",use_database);
            }else if(status == 0){
                strcpy(message,"permission denied");
                close(*new_socket);
            }
            send(*new_socket , message , strlen(message) , 0 );
            valread = recv( *new_socket , buffer, 1024, 0);
            //generate command
            dump_commands(new_socket, use_database);
        }


        close(*new_socket);
        return 0;
    }
    while(1){
        char buffer[BUFFLEN] = {0};
        char message[BUFFLEN] = {0};
        bool select_succeed = false;

        valread = recv( *new_socket , buffer, BUFFLEN, 0);

        if(is_disconnected(valread, new_socket)){
            printf("Client Shutdown\n");
            break;
        }

        if(!strncmp(buffer,"CREATE USER",11)){
            append_log(login_user, buffer);
            int status = create_user(buffer,tipe);
            switch (status){
            case 0:
                strcpy(message,"permission denied");
                break;
            case -1:
                strcpy(message,"syntax error");
                break;
            case -2:
                strcpy(message,"user already exist");
                break;
            case -3:
                strcpy(message, "missing semicolon");
                break;
            case 1:
                strcpy(message,"create user success");
                break;
            }

        }else if(!strncmp(buffer,"USE",3)){
            append_log(login_user, buffer);
            int status = use(buffer, tipe, login_user, use_database);
            switch (status){
            case 0:
                strcpy(message,"permission denied");
                break;
            case -1:
                strcpy(message,"syntax error");
                break;
            case -2:
                strcpy(message,"unknown database");
                break;
            case -3:
                strcpy(message, "missing semicolon");
                break;                
            case 1:
                sprintf(message,"change database to %s success", use_database);
                break;
            }

        }else if(!strncmp(buffer, "CREATE DATABASE", 15)){
            append_log(login_user, buffer);
            int status = create_database(buffer,tipe,login_user);
            switch (status){
            case 0:
                strcpy(message,"permission denied");
                break;
            case -1:
                strcpy(message,"syntax error");
                break;
            case -2:
                strcpy(message, "missing semicolon");
                break;
            case 1:
                strcpy(message,"create database success");
                break;
            }

        }else if(!strncmp(buffer,"GRANT PERMISSION",16)){
            append_log(login_user, buffer);
            int status = grant_pemission(buffer,tipe);
            switch (status){
            case 0:
                strcpy(message,"grant permission denied");
                break;
            case -1:
                strcpy(message,"syntax error");
                break;
            case -2:
                strcpy(message,"database not exist");
                break;
            case -3:
                strcpy(message,"unknown user");
                break;
            case -4:
                strcpy(message, "missing semicolon");
                break;            
            case 1:
                strcpy(message,"grant permission success");
                break;
            }

        }else if(!strncmp(buffer,"CREATE TABLE",12)){
            append_log(login_user, buffer);
            int status = create_table(buffer,use_database);
            switch (status){
            case 0:
                strcpy(message,"table already exist");
                break;
            case -1:
                strcpy(message,"syntax error");
                break;
            case -2:
                strcpy(message,"no database used");
                break;
            case -3:
                strcpy(message,"missing column type");
                break;
            case -4:
                strcpy(message,"invalid column type");
                break;
            case -5:
                strcpy(message, "missing semicolon");
                break;
            case 1:
                strcpy(message,"create table success");
                break;
            }

        }else if(!strncmp(buffer,"DROP DATABASE",13)){
            append_log(login_user, buffer);
            int status = drop_database(buffer,tipe,login_user,use_database);
            switch (status){
            case 0:
                strcpy(message,"permission denied");
                break;
            case -1:
                strcpy(message,"syntax error");
                break;
            case -2:
                strcpy(message,"unknown database");
                break;
            case -3:
                strcpy(message, "missing semicolon");
                break;                
            case 1:
                strcpy(message,"drop database success");
            }   

        }else if(!strncmp(buffer,"INSERT INTO",11)){
            append_log(login_user, buffer);
            int status = insert(buffer,use_database);
            switch (status) {
            case -1:
                strcpy(message,"no database used");
                break;
            case -2:
                strcpy(message,"table does not exist");
                break;
            case -3:
                strcpy(message,"coloumn count doesnt match");
                break;
            case -4:
                strcpy(message,"invalid input");
                break;
            case -5:
                strcpy(message, "missing semicolon");
                break;                
            case 1:
                strcpy(message,"Insert success");
                break;
            }

        }else if(!strncmp(buffer,"DROP TABLE",10)){
            append_log(login_user, buffer);
            int status = drop_table(buffer,use_database);
            switch (status) {
            case -1:
                strcpy(message,"table does not exist");
                break;
            case -2:
                strcpy(message,"no database used");
                break;
            case -3:
                strcpy(message, "missing semicolon");
                break;                     
            case 1:
                strcpy(message,"drop table success");
                break;
            }

        }else if(!strncmp(buffer,"DROP COLUMN",11)){
            append_log(login_user, buffer);
            int status = drop_column(buffer,use_database);
            switch (status) {
            case -1:
                strcpy(message,"table does not exist");
                break;
            case -2:
                strcpy(message,"no database used");
                break;
            case -3:
                strcpy(message,"invalid syntax");
                break;
            case -4:
                strcpy(message, "missing semicolon");
                break;
            case 1:
                strcpy(message,"drop column success");
                break;
            }

        }else if(!strncmp(buffer,"DELETE FROM",11)){
            int status = delete_from(buffer,use_database);
            switch(status){
                case -1:
                    strcpy(message,"no database used");
                    break;
                case -2:
                    strcpy(message,"invalid syntax");
                    break;
                case -3:
                    strcpy(message,"table does not exist");
                    break;
                case -4:
                    strcpy(message,"column does not exist");
                    break;
                case -5:
                    strcpy(message, "missing semicolon");
                    break;                    
                case 1:
                    strcpy(message,"delete success");
                    break;
            }

        }else if(!strncmp(buffer,"UPDATE",6)){
            int status = update(buffer,use_database);
            switch(status){
                case -1:
                    strcpy(message,"no database used");
                    break;
                case -2:
                    strcpy(message,"invalid syntax");
                    break;
                case -3:
                    strcpy(message,"table does not exist");
                    break;
                case -4:
                    strcpy(message,"column does not exist");
                    break;
                case -5:
                    strcpy(message, "missing semicolon");
                    break;   
                case 1:
                    strcpy(message,"update success");
                    break;
            }

        }else if(!strncmp(buffer, "SELECT", 6)){
            int status = select_table(buffer, use_database, new_socket);
            switch(status){
                case -1:
                    strcpy(message,"no database used");
                    break;
                case -2:
                    strcpy(message,"invalid syntax");
                    break;
                case -3:
                    strcpy(message,"table does not exist");
                    break;
                case -4:
                    strcpy(message,"column does not exist");
                    break;
                case -5:
                    strcpy(message, "missing semicolon");
                    break;
                case 1:
                    strcpy(message, "select success");
                    break;
            }

        }else{
            append_log(login_user, buffer);
            strcpy(message, "command not found");
        }

        printf("server reply: %s\n", message);
        send(*new_socket , message , strlen(message) , 0 );
    }
}

int main(int argc, char const *argv[]) {

    char dbpath[BUFFLEN] = {0};
    sprintf(dbpath,"%s/databases",workspace);
    mkdir(dbpath, 0777);

    int server_fd, new_socket[MAX_CLIENTS];
    struct sockaddr_in address;
    int opt = 1;
    int addrlen = sizeof(address);
      
    if ((server_fd = socket(AF_INET, SOCK_STREAM, 0)) == 0) {
        perror("socket failed");
        exit(EXIT_FAILURE);
    }
      
    if (setsockopt(server_fd, SOL_SOCKET, SO_REUSEADDR | SO_REUSEPORT, &opt, sizeof(opt))) {
        perror("setsockopt");
        exit(EXIT_FAILURE);
    }

    address.sin_family = AF_INET;
    address.sin_addr.s_addr = INADDR_ANY;
    address.sin_port = htons( PORT );
      
    if (bind(server_fd, (struct sockaddr *)&address, sizeof(address))<0) {
        perror("bind failed");
        exit(EXIT_FAILURE);
    }

    if (listen(server_fd, 3) < 0) {
        perror("listen");
        exit(EXIT_FAILURE);
    }
    set_server_daemon();
    int ctr = 0;
    while(1){
        if ((new_socket[ctr] = accept(server_fd, (struct sockaddr *)&address, (socklen_t*)&addrlen))<0) {
            perror("accept\n");
            exit(EXIT_FAILURE);
        }
        pthread_create(&(tid[ctr]),NULL,play,&new_socket[ctr]);
        ctr++;
        printf("Client %d terhubung\n", ctr);
    }
    return 0;
}